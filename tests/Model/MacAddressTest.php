<?php

declare(strict_types=1);

namespace Tests\Model;

use Garrcomm\Netutils\Model\MacAddress;
use PHPUnit\Framework\TestCase;
use InvalidArgumentException;

/**
 * Test for the MacAddress model
 *
 * @package App\Tests
 */
class MacAddressTest extends TestCase
{
    /**
     * Test data for the IPv4 Address type
     *
     * This data provider can be used for multiple tests; each row has data that matches the complete row.
     *
     * @return array{string, string, int, string}[]
     */
    public function macAddressValidTestData(): array
    {
        return array(
            // macAddress    separator Casing                 Result
            ['7828ca70bf0a', ':',      MacAddress::UPPERCASE, '78:28:CA:70:BF:0A'],
            ['7828ca6caeda', '',       MacAddress::LOWERCASE, '7828ca6caeda'],
            ['7828ca6caec2', '-',      MacAddress::LOWERCASE, '78-28-ca-6c-ae-c2'],
        );
    }

    /**
     * This data provider returns a list of invalid MAC addresses
     *
     * @return array<string, string[]>
     */
    public function macAddressInvalidTestData(): array
    {
        return array(
            '12 characters but not all hexadecimal'        => ['abcdefghijkl'],
            'All hexadecimal, but more then 12 characters' => ['abcdef1234567890'],
            'All hexadecimal, but less then 12 characters' => ['abcdef123'],
        );
    }

    /**
     * Test for the formatting function
     *
     * @param string  $macAddress The input MAC address without any separator character.
     * @param string  $separator  A separator character.
     * @param integer $casing     Should it be formatted upper or lower case.
     * @param string  $result     The result.
     *
     * @dataProvider macAddressValidTestData
     *
     * @return void
     */
    public function testFormatter(string $macAddress, string $separator, int $casing, string $result): void
    {
        $MacAddress = new MacAddress($macAddress);
        $this->assertEquals($result, $MacAddress->format($separator, $casing));
    }

    /**
     * Tests string casting
     *
     * @param string $macAddress The input MAC address without any separator character.
     *
     * @dataProvider macAddressValidTestData
     *
     * @return void
     */
    public function testToString(string $macAddress): void
    {
        $MacAddress = new MacAddress($macAddress);
        $this->assertEquals(strtoupper($macAddress), $MacAddress);
    }

    /**
     * Test for json encoding
     *
     * @param string $macAddress The input MAC address without any separator character.
     *
     * @dataProvider macAddressValidTestData
     *
     * @return void
     */
    public function testJsonEncode(string $macAddress): void
    {
        $MacAddress = new MacAddress($macAddress);
        $this->assertEquals(
            '"' . $MacAddress->format(':', MacAddress::LOWERCASE) . '"',
            json_encode($MacAddress)
        );
    }

    /**
     * Test for invalid formats
     *
     * @param string $macAddress The input MAC address without any separator character.
     *
     * @dataProvider macAddressInvalidTestData
     *
     * @return void
     */
    public function testInvalids(string $macAddress): void
    {
        $this->expectException(\InvalidArgumentException::class);
        new MacAddress($macAddress);
    }

    /**
     * Tests the __set_state method
     *
     * @param string  $macAddress The input MAC address without any separator character.
     * @param string  $separator  A separator character.
     * @param integer $casing     Should it be formatted upper or lower case.
     * @param string  $result     The result.
     *
     * @dataProvider macAddressValidTestData
     *
     * @return void
     */
    public function testSetState(string $macAddress, string $separator, int $casing, string $result): void
    {
        $object = MacAddress::__set_state(['macAddress' => $macAddress]);
        $this->assertEquals($result, $object->format($separator, $casing));
    }

    /**
     * Tests an invalid state
     *
     * @return void
     */
    public function testInvalidSetState(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('No macAddress found in the state');
        MacAddress::__set_state([]);
    }
}
