<?php

namespace Tests\Model;

use Garrcomm\Netutils\Model\Ipv4Address;
use PHPUnit\Framework\TestCase;
use Garrcomm\Netutils\Model\Ipv4Range;
use InvalidArgumentException;
use RuntimeException;

class Ipv4RangeTest extends TestCase
{
    /**
     * Returns test data for valid IP ranges
     *
     * @return array<string, array{networkAddress: string, bitMask: int, range: string[]}>
     */
    public function ipRangeDataProvider(): array
    {
        $range1921682x24 = array();
        for ($iterator = 0; $iterator < 256; ++$iterator) {
            $range1921682x24[] = '192.168.2.' . $iterator;
        }

        return array(
            '192.168.2.0/24 - A regular IP range' => [
                'networkAddress' => '192.168.2.0',
                'bitMask' => 24,
                'range' => $range1921682x24,
                'string' => '192.168.2.0/24-192.168.2.255/24',
            ],
            '192.168.2.230/24 - IP Address should be converted to Network Address' => [
                'networkAddress' => '192.168.2.230',
                'bitMask' => 24,
                'range' => $range1921682x24,
                'string' => '192.168.2.0/24-192.168.2.255/24',
            ],
            '192.168.32.12/30 - A small IP range' => [
                'networkAddress' => '192.168.32.12',
                'bitMask' => 30,
                'range' => [
                    '192.168.32.12',
                    '192.168.32.13',
                    '192.168.32.14',
                    '192.168.32.15',
                ],
                'string' => '192.168.32.12/30-192.168.32.15/30',
            ],
        );
    }

    /**
     * Tests the constructor with an invalid network address
     *
     * @return void
     */
    public function testInvalidConstruct(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('networkAddress must be a string or an integer');
        new Ipv4Range('foobar', 24);
    }

    /**
     * Tests the Iterator interface
     *
     * @param string   $networkAddress The network address.
     * @param integer  $bitMask        The bitmask.
     * @param string[] $range          The resulting IP range.
     *
     * @return void
     *
     * @dataProvider ipRangeDataProvider
     */
    public function testIterator(string $networkAddress, int $bitMask, array $range): void
    {
        $ipv4Range = new Ipv4Range($networkAddress, $bitMask);
        foreach ($ipv4Range as $iterator => $ipAddress) {
            $this->assertInstanceOf(Ipv4Address::class, $ipAddress);
            $this->assertEquals($range[$iterator], $ipAddress->getIpAddress());
            $this->assertEquals($bitMask, $ipAddress->getBitMask());
        }

        // After the loop, there's nothing left
        $ipv4Range->next();
        $this->assertNull($ipv4Range->current());
    }

    /**
     * Tests the ArrayAccess and Countable interfaces
     *
     * @param string   $networkAddress The network address.
     * @param integer  $bitMask        The bitmask.
     * @param string[] $range          The resulting IP range.
     *
     * @return void
     *
     * @dataProvider ipRangeDataProvider
     */
    public function testArrayAccessCountable(string $networkAddress, int $bitMask, array $range): void
    {
        $ipv4Range = new Ipv4Range($networkAddress, $bitMask);
        for ($iterator = 0; $iterator < count($ipv4Range); ++$iterator) {
            $this->assertInstanceOf(Ipv4Address::class, $ipv4Range[$iterator]);
            $this->assertEquals($range[$iterator], $ipv4Range[$iterator]->getIpAddress());
            $this->assertEquals($bitMask, $ipv4Range[$iterator]->getBitMask());
        }

        // After the loop, there's nothing left
        $this->assertNull($ipv4Range[count($ipv4Range)]);
    }

    /**
     * Tests the __set_state and __toString methods
     *
     * @param string   $networkAddress The network address.
     * @param integer  $bitMask        The bitmask.
     * @param string[] $range          The resulting IP range.
     * @param string   $string         The __toString result.
     *
     * @return void
     *
     * @dataProvider ipRangeDataProvider
     */
    public function testSetStateToString(string $networkAddress, int $bitMask, array $range, string $string): void
    {
        $ipv4Range = Ipv4Range::__set_state([
            'networkAddress' => $networkAddress,
            'bitMask' => $bitMask,
            'position' => 1,
        ]);
        $this->assertEquals($string, (string)$ipv4Range);
        $this->assertInstanceOf(Ipv4Address::class, $ipv4Range->current());
        $this->assertEquals($range[1], $ipv4Range->current()->getIpAddress());
    }

    /**
     * Tests the __set_state without network address
     *
     * @return void
     */
    public function testSetStateNoNetworkAddress(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('No networkAddress found in the state');
        Ipv4Range::__set_state([]);
    }

    /**
     * Tests the __set_state without bitmask
     *
     * @return void
     */
    public function testSetStateNoBitMask(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('No bitMask found in the state');
        Ipv4Range::__set_state(['networkAddress' => '192.168.2.0']);
    }

    /**
     * Tests the OffsetSet error message
     *
     * @return void
     */
    public function testOffsetSet(): void
    {
        $ipv4Range = new Ipv4Range('192.168.2.0', 24);
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('The ' . Ipv4Range::class . ' object is read only');
        $ipv4Range[1] = 'foo';
    }

    /**
     * Tests the OffsetUnset error message
     *
     * @return void
     */
    public function testOffsetUnset(): void
    {
        $ipv4Range = new Ipv4Range('192.168.2.0', 24);
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('The ' . Ipv4Range::class . ' object is read only');
        unset($ipv4Range[1]);
    }

    /**
     * Tests the JsonSerializable interface
     *
     * @param string   $networkAddress The network address.
     * @param integer  $bitMask        The bitmask.
     * @param string[] $range          The resulting IP range.
     *
     * @return void
     *
     * @dataProvider ipRangeDataProvider
     */
    public function testJsonSerializable(string $networkAddress, int $bitMask, array $range): void
    {
        $compareArray = array_map(function ($value) use ($bitMask) {
            return $value . '/' . $bitMask;
        }, $range);

        $ipv4Range = new Ipv4Range($networkAddress, $bitMask);
        $this->assertEquals($compareArray, $ipv4Range->jsonSerialize());
    }
}
