<?php

declare(strict_types=1);

namespace Tests\Model;

use Garrcomm\Netutils\Model\Url;
use PHPUnit\Framework\TestCase;

class UrlTest extends TestCase
{
    /**
     * Backup for the original $_SERVER global
     *
     * @var string[]
     */
    protected $originalServerGlobal;

    /**
     * This method is called before each test.
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $this->originalServerGlobal = $_SERVER;
    }

    /**
     * This method is called after each test.
     *
     * @return void
     */
    public function tearDown(): void
    {
        parent::tearDown();
        $_SERVER = $this->originalServerGlobal;
    }

    /**
     * Returns test data for testCurrent()
     *
     * @return array<string, array{serverGlobal: string[], stringResult: string}>
     */
    public function currentDataProvider(): array
    {
        return array(
            'Simple URL' => [
                'serverGlobal' => [
                    'HTTP_HOST' => 'localhost',
                    'SERVER_PORT' => '80',
                    'REQUEST_URI' => '/foo/bar?baz=123'
                ],
                'stringResult' => 'http://localhost/foo/bar?baz=123',
            ],
            'Simple URL with credentials' => [
                'serverGlobal' => [
                    'HTTP_HOST' => 'localhost',
                    'SERVER_PORT' => '80',
                    'REQUEST_URI' => '/foo/bar?baz=123',
                    'PHP_AUTH_USER' => 'foo',
                    'PHP_AUTH_PW' => 'bar',
                ],
                'stringResult' => 'http://foo:bar@localhost/foo/bar?baz=123',
            ],
            'Simple URL with only a username' => [
                'serverGlobal' => [
                    'HTTP_HOST' => 'localhost',
                    'SERVER_PORT' => '80',
                    'REQUEST_URI' => '/foo/bar?baz=123',
                    'PHP_AUTH_USER' => 'foo',
                ],
                'stringResult' => 'http://foo@localhost/foo/bar?baz=123',
            ],
            'HTTPS URL' => [
                'serverGlobal' => [
                    'HTTP_HOST' => 'localhost',
                    'HTTPS' => 'on',
                    'SERVER_PORT' => '443',
                    'REQUEST_URI' => '/foo/bar?baz=123'
                ],
                'stringResult' => 'https://localhost/foo/bar?baz=123',
            ],
            'Alternative HTTP port with SERVER_PORT' => [
                'serverGlobal' => [
                    'HTTP_HOST' => 'localhost',
                    'SERVER_PORT' => '8080',
                    'REQUEST_URI' => '/foo/bar?baz=123'
                ],
                'stringResult' => 'http://localhost:8080/foo/bar?baz=123',
            ],
            'Alternative HTTP port within HTTP_HOST' => [
                'serverGlobal' => [
                    'HTTP_HOST' => 'localhost:8080',
                    'REQUEST_URI' => '/foo/bar?baz=123'
                ],
                'stringResult' => 'http://localhost:8080/foo/bar?baz=123',
            ],
            'Alternative HTTPS port with SERVER_PORT' => [
                'serverGlobal' => [
                    'HTTP_HOST' => 'localhost',
                    'HTTPS' => 'on',
                    'SERVER_PORT' => '8443',
                    'REQUEST_URI' => '/foo/bar?baz=123'
                ],
                'stringResult' => 'https://localhost:8443/foo/bar?baz=123',
            ],
            'Alternative HTTPS port within HTTP_HOST' => [
                'serverGlobal' => [
                    'HTTP_HOST' => 'localhost:8443',
                    'HTTPS' => 'on',
                    'REQUEST_URI' => '/foo/bar?baz=123'
                ],
                'stringResult' => 'https://localhost:8443/foo/bar?baz=123',
            ],
        );
    }

    /**
     * Dataprovider for testRemoveQueryData
     *
     * @return array<string, array{beforeUrl: string, removeKey: string, afterUrl: string}>
     */
    public function removeQueryDataProvider(): array
    {
        return array(
            'Simple URL' => [
                'beforeUrl' => 'http://localhost/foo?bar=baz',
                'removeKey' => 'bar',
                'afterUrl' => 'http://localhost/foo',
            ],
            'Key in the middle' => [
                'beforeUrl' => 'http://localhost/foo?one=two&bar=baz&three=four',
                'removeKey' => 'bar',
                'afterUrl' => 'http://localhost/foo?one=two&three=four',
            ],
            'Key in the middle with fragment' => [
                'beforeUrl' => 'http://localhost/foo?one=two&bar=baz&three=four#fragment-here',
                'removeKey' => 'bar',
                'afterUrl' => 'http://localhost/foo?one=two&three=four#fragment-here',
            ],
            'Key doesn\'t exist' => [
                'beforeUrl' => 'http://localhost/foo',
                'removeKey' => 'bar',
                'afterUrl' => 'http://localhost/foo',
            ],
        );
    }

    /**
     * Dataprovider for testSetQueryData
     *
     * @return array<string, array{beforeUrl: string, key: string, value: string|mixed, afterUrl: string}>
     */
    public function setQueryDataProvider(): array
    {
        return array(
            'Simple URL' => [
                'beforeUrl' => 'http://localhost/foo?bar=baz',
                'key' => 'bar',
                'value' => 'foo',
                'afterUrl' => 'http://localhost/foo?bar=foo',
            ],
            'Simple URL with array in query' => [
                'beforeUrl' => 'http://localhost/?foo%5B0%5D=bar&foo%5B0%5D=baz',
                'key' => 'foo',
                'value' => ['abc', 'def'],
                'afterUrl' => 'http://localhost/?foo%5B0%5D=abc&foo%5B1%5D=def',
            ],
            'Simple URL with credentials' => [
                'beforeUrl' => 'http://foo:bar@localhost/foo?bar=baz',
                'key' => 'bar',
                'value' => 'foo',
                'afterUrl' => 'http://foo:bar@localhost/foo?bar=foo',
            ],
            'Simple URL with a user only' => [
                'beforeUrl' => 'http://foo@localhost/foo?bar=baz',
                'key' => 'bar',
                'value' => 'foo',
                'afterUrl' => 'http://foo@localhost/foo?bar=foo',
            ],
            'Simple URL but custom port' => [
                'beforeUrl' => 'http://localhost:8080/foo?bar=baz',
                'key' => 'bar',
                'value' => 'foo',
                'afterUrl' => 'http://localhost:8080/foo?bar=foo',
            ],
            'Key in the middle' => [
                'beforeUrl' => 'http://localhost/foo?one=two&bar=baz&three=four',
                'key' => 'bar',
                'value' => 'foo',
                'afterUrl' => 'http://localhost/foo?one=two&bar=foo&three=four',
            ],
            'New key on blank URL with question mark in fragment' => [
                'beforeUrl' => 'http://localhost/foo#fragment?here',
                'key' => 'bar',
                'value' => 'foo',
                'afterUrl' => 'http://localhost/foo?bar=foo#fragment?here',
            ],
            'Query string with unicode characters' => [
                'beforeUrl' => 'http://localhost/foo',
                'key' => 'foo',
                'value' => 'Contains ûñïçôďé characters',
                'afterUrl' => 'http://localhost/foo?foo=Contains+%C3%BB%C3%B1%C3%AF%C3%A7%C3%B4%C4%8F%C3%A9+characters',
            ],
        );
    }

    /**
     * Testdata for getQueryData (based on setQueryData with some additions)
     *
     * @return array<string, array{url: string, key: string, value: string|mixed|null}>
     */
    public function getQueryDataProvider(): array
    {
        $return = array(
            'Non existent query item' => [
                'url' => 'http://localhost/?foo=bar',
                'key' => 'baz',
                'value' => null,
            ],
            'No query' => [
                'url' => 'http://localhost/',
                'key' => 'baz',
                'value' => null,
            ],
            'Array in query' => [
                'url' => 'http://localhost/?foo%5B%5D=bar&foo%5B%5D=baz',
                'key' => 'foo',
                'value' => ['bar', 'baz'],
            ],
        );
        foreach ($this->setQueryDataProvider() as $key => $data) {
            $return[$key] = [
                'url' => $data['afterUrl'],
                'key' => $data['key'],
                'value' => $data['value'],
            ];
        }
        return $return;
    }

    /**
     * Dataprovider for the setFullQuery tests
     *
     * @return array<string, array{beforeUrl: string, newQuery: array<string, mixed>, afterUrl: string}>
     */
    public function setFullQueryDataProvider(): array
    {
        return array(
            'Simple URL' => [
                'beforeUrl' => 'http://localhost/?foo=bar',
                'newQuery' => ['bar' => 'baz'],
                'afterUrl' => 'http://localhost/?bar=baz',
            ],
            'Clear the query' => [
                'beforeUrl' => 'http://localhost/?foo=bar',
                'newQuery' => [],
                'afterUrl' => 'http://localhost/',
            ],
            'Array within the query' => [
                'beforeUrl' => 'http://localhost/?foo=bar',
                'newQuery' => ['foo' => ['bar', 'baz']],
                'afterUrl' => 'http://localhost/?foo%5B0%5D=bar&foo%5B1%5D=baz',
            ],
            'Query with unicode characters' => [
                'beforeUrl' => 'http://localhost/?foo=bar',
                'newQuery' => ['test' => 'Contains ûñïçôďé characters'],
                'afterUrl' => 'http://localhost/?test=Contains+%C3%BB%C3%B1%C3%AF%C3%A7%C3%B4%C4%8F%C3%A9+characters',
            ],
        );
    }

    /**
     * Returns data for the getFullQuery test
     *
     * @return array<string, array{url: string, query: array<string, mixed>}>
     */
    public function getFullQueryDataProvider(): array
    {
        $return = array();
        foreach ($this->setFullQueryDataProvider() as $key => $data) {
            $return[$key] = [
                'url' => $data['afterUrl'],
                'query' => $data['newQuery'],
            ];
        }
        return $return;
    }

    /**
     * Test dataset for getFragment; a copy from setFragment with some additions
     *
     * @return array<string, array{url: string, fragment: string|null}>
     */
    public function getFragmentDataProvider(): array
    {
        $return = array(
            'No fragment' => [
                'url' => 'https://localhost/foo?bar=baz',
                'fragment' => null,
            ],
        );
        foreach ($this->setFragmentDataProvider() as $key => $data) {
            $return[$key] = [
                'url' => $data['afterUrl'],
                'fragment' => $data['newFragment'],
            ];
        }
        return $return;
    }

    /**
     * Dataprovider for testSetFragment
     *
     * @return array<string, array{beforeUrl: string, newFragment: string, afterUrl: string}>
     */
    public function setFragmentDataProvider(): array
    {
        return array(
            'Simple URL' => [
                'beforeUrl' => 'http://localhost/foo#This-is-the-fragment',
                'newFragment' => 'foo-bar',
                'afterUrl' => 'http://localhost/foo#foo-bar',
            ],
            'Simple URL with credentials' => [
                'beforeUrl' => 'http://foo:bar@localhost/foo#This-is-the-fragment',
                'newFragment' => 'foo-bar',
                'afterUrl' => 'http://foo:bar@localhost/foo#foo-bar',
            ],
            'Simple URL with a user only' => [
                'beforeUrl' => 'http://foo@localhost/foo#This-is-the-fragment',
                'newFragment' => 'foo-bar',
                'afterUrl' => 'http://foo@localhost/foo#foo-bar',
            ],
            'Simple URL with different port' => [
                'beforeUrl' => 'http://localhost:8080/foo#This-is-the-fragment',
                'newFragment' => 'foo-bar',
                'afterUrl' => 'http://localhost:8080/foo#foo-bar',
            ],
            'URL without fragment' => [
                'beforeUrl' => 'http://localhost/foo',
                'newFragment' => 'foo-bar',
                'afterUrl' => 'http://localhost/foo#foo-bar',
            ],
            'URL with query string and fragment' => [
                'beforeUrl' => 'http://localhost/foo?foo=bar#This-is-the-fragment',
                'newFragment' => 'foo-bar',
                'afterUrl' => 'http://localhost/foo?foo=bar#foo-bar',
            ],
            'Remove the fragment' => [
                'beforeUrl' => 'http://localhost/foo#This-is-the-fragment',
                'newFragment' => '',
                'afterUrl' => 'http://localhost/foo',
            ],
            'URL with unicode characters' => [
                'beforeUrl' => 'http://localhost/foo',
                'newFragment' => 'Contains ûñïçôďé characters',
                'afterUrl' => 'http://localhost/foo#Contains%20%C3%BB%C3%B1%C3%AF%C3%A7%C3%B4%C4%8F%C3%A9%20characters',
            ],
        );
    }

    /**
     * Dataprovider for testRemoveFragmentData
     *
     * @return array<string, array{beforeUrl: string, afterUrl: string}>
     */
    public function removeFragmentDataProvider(): array
    {
        return array(
            'Simple URL' => [
                'beforeUrl' => 'https://localhost/foo#This-is-a-fragment',
                'afterUrl' => 'https://localhost/foo',
            ],
        );
    }

    /**
     * Data provider for testJsonSerialize
     *
     * @return array<string, array{originalUrl: string, jsonResult: string}>
     */
    public function jsonSerializeDataProvider(): array
    {
        return array(
            'Simple URL' => [
                'originalUrl' => 'https://localhost:8443/foo/bar?baz=123',
                'jsonResult' => '"https:\/\/localhost:8443\/foo\/bar?baz=123"',
            ],
        );
    }

    /**
     * Dataprovider for the setHostname tests
     *
     * @return array<string, array{beforeUrl: string, newHost: string, afterUrl: string}>
     */
    public function setHostnameDataProvider(): array
    {
        return array(
            'Simple URL' => [
                'beforeUrl' => 'http://localhost/foo',
                'newHost' => 'bar.baz',
                'afterUrl' => 'http://bar.baz/foo',
            ],
            'URL with port' => [
                'beforeUrl' => 'http://localhost:8080/bar',
                'newHost' => 'bar.baz',
                'afterUrl' => 'http://bar.baz:8080/bar',
            ],
            'URL with credentials' => [
                'beforeUrl' => 'http://user:pass@localhost:8080/bar',
                'newHost' => 'bar.baz',
                'afterUrl' => 'http://user:pass@bar.baz:8080/bar',
            ],
        );
    }

    /**
     * Dataprovider for the getHostname tests
     *
     * @return array<string, array{url: string, hostname: string}>
     */
    public function getHostnameDataProvider(): array
    {
        $return = array();
        foreach ($this->setHostnameDataProvider() as $key => $data) {
            $return[$key] = [
                'url' => $data['afterUrl'],
                'hostname' => $data['newHost'],
            ];
        }
        return $return;
    }

    /**
     * Test data for setPath
     *
     * @return array<string, array{beforeUrl: string, newPath: string, afterUrl: string}>
     */
    public function setPathDataProvider(): array
    {
        return array(
            'Simple URL' => [
                'beforeUrl' => 'http://localhost/foo',
                'newPath' => '/bar',
                'afterUrl' => 'http://localhost/bar',
            ],
        );
    }

    /**
     * Test data for getPath
     *
     * @return array<string, array{url: string, path: string}>
     */
    public function getPathDataProvider(): array
    {
        $return = array();
        foreach ($this->setPathDataProvider() as $key => $data) {
            $return[$key] = [
                'url' => $data['afterUrl'],
                'path' => $data['newPath'],
            ];
        }
        return $return;
    }

    /**
     * Tests the Url::current() method
     *
     * @param string[] $serverGlobal The $_SERVER global content.
     * @param string   $stringResult The URL result.
     *
     * @dataProvider currentDataProvider
     *
     * @return void
     */
    public function testCurrent(array $serverGlobal, string $stringResult): void
    {
        $_SERVER = $serverGlobal;
        $url = Url::current();
        $this->assertEquals($stringResult, (string)$url);
    }

    /**
     * Tests the scenario where current() can't detect a web request
     *
     * @return void
     */
    public function testCurrentForConsoleRequests(): void
    {
        // This shouldn't be necessary since phpunit is run from the console, but just to be sure we'll unset these.
        if (isset($_SERVER['HTTP_HOST'])) {
            unset($_SERVER['HTTP_HOST']);
        }
        if (isset($_SERVER['REQUEST_URI'])) {
            unset($_SERVER['REQUEST_URI']);
        }
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionMessage('The current request is not a web request');
        Url::current();
    }

    /**
     * Tests the Url->jsonSerialize() method
     *
     * @param string $originalUrl The original URL.
     * @param string $jsonResult  The serialized result.
     *
     * @dataProvider jsonSerializeDataProvider
     *
     * @return void
     */
    public function testJsonSerialize(string $originalUrl, string $jsonResult): void
    {
        $url = new Url($originalUrl);
        $this->assertEquals($jsonResult, json_encode($url));
    }

    /**
     * Tests the removeQuery method
     *
     * @param string $beforeUrl The URL before the change.
     * @param string $removeKey The key that needs to be removed.
     * @param string $afterUrl  The URL after the change.
     *
     * @dataProvider removeQueryDataProvider
     *
     * @return void
     */
    public function testRemoveQuery(string $beforeUrl, string $removeKey, string $afterUrl): void
    {
        $url = new Url($beforeUrl);
        $this->assertEquals($afterUrl, (string)$url->removeQuery($removeKey));
    }

    /**
     * Tests the setQuery method
     *
     * @param string       $beforeUrl The URL before the change.
     * @param string       $key       The query string key.
     * @param string|mixed $value     The new value.
     * @param string       $afterUrl  The URL after the change.
     *
     * @dataProvider setQueryDataProvider
     *
     * @return void
     */
    public function testSetQuery(string $beforeUrl, string $key, $value, string $afterUrl): void
    {
        $url = new Url($beforeUrl);
        $this->assertEquals($afterUrl, (string)$url->setQuery($key, $value));
    }

    /**
     * Tests the getQuery method
     *
     * @param string            $url   The URL.
     * @param string            $key   Query string key.
     * @param string|mixed|null $value Query string value.
     *
     * @return void
     *
     * @dataProvider getQueryDataProvider
     */
    public function testGetQuery(string $url, string $key, $value): void
    {
        $urlObject = new Url($url);
        $this->assertEquals($value, $urlObject->getQuery($key));
    }

    /**
     * Tests the setFullQuery method
     *
     * @param string               $beforeUrl The URL before the change.
     * @param array<string, mixed> $newQuery  The new query data.
     * @param string               $afterUrl  The URL after the change.
     *
     * @return void
     *
     * @dataProvider setFullQueryDataProvider
     */
    public function testSetFullQuery(string $beforeUrl, array $newQuery, string $afterUrl): void
    {
        $url = new Url($beforeUrl);
        $url->setFullQuery($newQuery);
        $this->assertEquals($afterUrl, (string)$url);
    }

    /**
     * Tests the getFullQuery method
     *
     * @param string               $url   The full URL.
     * @param array<string, mixed> $query The expected query data.
     *
     * @return void
     *
     * @dataProvider getFullQueryDataProvider
     */
    public function testGetFullQuery(string $url, array $query): void
    {
        $urlObject = new Url($url);
        $this->assertEquals($query, $urlObject->getFullQuery());
    }

    /**
     * Tests the setQuery method
     *
     * @param string $beforeUrl   The URL before the change.
     * @param string $newFragment The new fragment.
     * @param string $afterUrl    The URL after the change.
     *
     * @dataProvider setFragmentDataProvider
     *
     * @return void
     */
    public function testSetFragment(string $beforeUrl, string $newFragment, string $afterUrl): void
    {
        $url = new Url($beforeUrl);
        $this->assertEquals($afterUrl, (string)$url->setFragment($newFragment));
    }

    /**
     * Tests the getFragment method
     *
     * @param string      $url      The URL.
     * @param string|null $fragment The fragment.
     *
     * @return void
     *
     * @dataProvider getFragmentDataProvider
     */
    public function testGetFragment(string $url, ?string $fragment): void
    {
        $urlObj = new Url($url);
        $this->assertEquals($fragment, $urlObj->getFragment());
    }

    /**
     * Tests the removeFragment method
     *
     * @param string $beforeUrl The URL before the change.
     * @param string $afterUrl  The URL after the change.
     *
     * @dataProvider removeFragmentDataProvider
     *
     * @return void
     */
    public function testRemoveFragment(string $beforeUrl, string $afterUrl): void
    {
        $url = new Url($beforeUrl);
        $this->assertEquals($afterUrl, (string)$url->removeFragment());
    }

    /**
     * Test for RemoveFullQuery
     *
     * @return void
     */
    public function testRemoveFullQuery(): void
    {
        $url = new Url('http://localhost/?foo=bar&baz=abc');
        $this->assertEquals('http://localhost/', (string)$url->removeFullQuery());
    }

    /**
     * Test for the setHostname method
     *
     * @param string $beforeUrl   The URL before the change.
     * @param string $newHostname The new hostname.
     * @param string $afterUrl    The URL after the change.
     *
     * @return void
     *
     * @dataProvider setHostnameDataProvider
     */
    public function testSetHostname(string $beforeUrl, string $newHostname, string $afterUrl): void
    {
        $url = new Url($beforeUrl);
        $this->assertEquals($afterUrl, (string)$url->setHostname($newHostname));
    }

    /**
     * Test for the getHostname method
     *
     * @param string $url      The full URL.
     * @param string $hostname The hostname.
     *
     * @return void
     *
     * @dataProvider getHostnameDataProvider
     */
    public function testGetHostname(string $url, string $hostname): void
    {
        $urlObject = new Url($url);
        $this->assertEquals($hostname, $urlObject->getHostname());
    }

    /**
     * Tests the missing scheme error
     *
     * @return void
     */
    public function testNoScheme(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('The specified URL is missing it\'s scheme');
        new Url('foobar/baz');
    }

    /**
     * Tests the auto detect http scheme
     *
     * @return void
     */
    public function testDetectHttpScheme(): void
    {
        $url = new Url('foobar:80/baz');
        $this->assertEquals('http', $url->getScheme());
    }

    /**
     * Tests the auto detect https scheme
     *
     * @return void
     */
    public function testDetectHttpsScheme(): void
    {
        $url = new Url('foobar:443/baz');
        $this->assertEquals('https', $url->getScheme());
    }

    /**
     * Tests setPath
     *
     * @param string $beforeUrl The URL before the change.
     * @param string $newPath   The new path.
     * @param string $afterUrl  The URL after the change.
     *
     * @return void
     *
     * @dataProvider setPathDataProvider
     */
    public function testSetPath(string $beforeUrl, string $newPath, string $afterUrl): void
    {
        $url = new Url($beforeUrl);
        $this->assertEquals($afterUrl, (string)$url->setPath($newPath));
    }

    /**
     * Tests getPath
     *
     * @param string $url  The URL.
     * @param string $path The path part.
     *
     * @return void
     *
     * @dataProvider getPathDataProvider
     */
    public function testGetPath(string $url, string $path): void
    {
        $urlObject = new Url($url);
        $this->assertEquals($path, $urlObject->getPath());
    }

    /**
     * Test constructing without a path
     *
     * @return void
     */
    public function testConstructorWithoutPath(): void
    {
        $url = new Url('http://localhost');
        $this->assertEquals('/', $url->getPath());
    }

    /**
     * Test setPath with invalid path
     *
     * @return void
     */
    public function testSetPathInvalid(): void
    {
        $url = new Url('http://localhost/foo');
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('The path must start with a /');
        $url->setPath('bar');
    }

    /**
     * Test for getPort and setPort
     *
     * @return void
     */
    public function testGetSetPort(): void
    {
        // Get port from URL
        $url = new Url('http://localhost:8080/');
        $this->assertEquals(8080, $url->getPort());
        // No port, so result should be null
        $url = new Url('http://localhost/');
        $this->assertNull($url->getPort());
        // Set the port to 180
        $url = new Url('http://localhost/');
        $this->assertEquals('http://localhost:180/', (string)$url->setPort(180));
    }

    /**
     * Test for removePort
     *
     * @return void
     */
    public function testRemovePort(): void
    {
        // Set the port from 8080 to empty
        $url = new Url('http://localhost:8080/');
        $this->assertEquals('http://localhost/', (string)$url->removePort());
        // The port already is empty
        $url = new Url('http://localhost/');
        $this->assertEquals('http://localhost/', (string)$url->removePort());
    }

    /**
     * Tests the get- set- and removeUsername
     *
     * @return void
     */
    public function testGetSetRemoveUsername(): void
    {
        $url = new Url('http://foo.bar/baz');
        $this->assertNull($url->getUsername());
        $this->assertEquals('http://user@foo.bar/baz', (string)$url->setUsername('user'));
        $this->assertEquals('http://user:pass@foo.bar/baz', (string)$url->setPassword('pass'));
        $this->assertEquals('http://foo.bar/baz', (string)$url->removeUsername());
    }

    /**
     * Tests the get, set and removePassword methods
     *
     * @return void
     */
    public function testGetSetRemovePassword(): void
    {
        $url = new Url('http://user@foo.bar/baz');
        $this->assertNull($url->getPassword());
        $this->assertEquals('http://user:pass@foo.bar/baz', (string)$url->setPassword('pass'));
        $this->assertEquals('http://user@foo.bar/baz', (string)$url->removePassword());
    }

    /**
     * Tests the __set_state method
     *
     * @return void
     */
    public function testSetState(): void
    {
        $url = Url::__set_state(array(
            'url' => 'http://localhost/',
        ));
        $this->assertEquals('http://localhost/', (string)$url);
    }

    /**
     * Tests the __set_state method
     *
     * @return void
     */
    public function testSetInvalidState(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('No URL found in the state');
        Url::__set_state([]);
    }
}
