<?php

namespace Tests\Model;

use Garrcomm\Netutils\Model\DnsAnswer;
use Garrcomm\Netutils\Model\DnsResult;
use Garrcomm\Netutils\Service\Dns;
use PHPUnit\Framework\TestCase;
use InvalidArgumentException;

class DnsResultTest extends TestCase
{
    /**
     * Returns test data for testSetStateIncomplete
     *
     * @return array<string, array{params:mixed[],missingKey:string}>
     */
    public function setStateIncompleteDataProvider(): array
    {
        $fullParams = [
            'question' => 'foo',
            'answers' => [],
            'dnsHostname' => 'baz',
            'dnsAddress' => '1.1.1.1:53',
        ];
        $return = array();
        foreach (array_keys($fullParams) as $missingKey) {
            $params = $fullParams;
            unset($params[$missingKey]);
            $return[$missingKey] = [
                'params' => $params,
                'missingKey' => $missingKey,
            ];
        }
        return $return;
    }

    /**
     * Tests the setState, jsonSerialize and __toString methods of the DnsResult and DnsAnswer classes.
     *
     * @return void
     */
    public function testSerialization(): void
    {
        // Constructs everything with __set_state
        $result = DnsResult::__set_state([
            'question' => 'example.com, type = NS, class = IN',
            'answers' => [
                DnsAnswer::__set_state([
                    'type' => 'nameserver',
                    'result' => 'b.iana-servers.net.',
                    'ttl' => 8672,
                    'priority' => null,
                ]),
                DnsAnswer::__set_state([
                    'type' => 'nameserver',
                    'result' => 'a.iana-servers.net.',
                    'ttl' => 8672,
                    'priority' => null,
                ]),
            ],
            'dnsHostname' => 'dns.google',
            'dnsAddress' => '8.8.8.8#53',
        ]);

        // DnsResult object
        $this->assertInstanceOf(DnsResult::class, $result);
        $this->assertEquals('dns.google', $result->getDnsHostname());
        $this->assertEquals('8.8.8.8#53', $result->getDnsAddress());
        $this->assertEquals('example.com, type = NS, class = IN', $result->getQuestion());
        $this->assertEquals("b.iana-servers.net.\na.iana-servers.net.", (string)$result);
        $this->assertCount(2, $result->getAnswers());

        // First DnsAnswer
        $this->assertEquals('b.iana-servers.net.', $result->getAnswers()[0]->getResult());
        $this->assertEquals('b.iana-servers.net.', (string)$result->getAnswers()[0]);
        $this->assertIsNumeric($result->getAnswers()[0]->getTtl());
        $this->assertNull($result->getAnswers()[0]->getPriority());
        $this->assertEquals(Dns::NS, $result->getAnswers()[0]->getType());

        // Second DnsAnswer
        $this->assertEquals('a.iana-servers.net.', $result->getAnswers()[1]->getResult());
        $this->assertEquals('a.iana-servers.net.', (string)$result->getAnswers()[1]);
        $this->assertIsNumeric($result->getAnswers()[1]->getTtl());
        $this->assertNull($result->getAnswers()[1]->getPriority());
        $this->assertEquals(Dns::NS, $result->getAnswers()[1]->getType());

        // DnsAnswers sorted (notice that previously it was first ba, now it's ab)
        $this->assertEquals('a.iana-servers.net.', $result->getAnswers(true)[0]->getResult());
        $this->assertEquals('b.iana-servers.net.', $result->getAnswers(true)[1]->getResult());

        // JSON encoded
        $this->assertEquals(
            '{"server":"dns.google","question":"example.com, type = NS, class = IN","answers":["b.iana-servers'
            . '.net.","a.iana-servers.net."]}',
            json_encode($result)
        );
    }

    /**
     * Tests missing dnsAddress argument
     *
     * @param mixed[] $params     The __set_state params.
     * @param string  $missingKey The missing key.
     *
     * @return void
     *
     * @dataProvider setStateIncompleteDataProvider
     */
    public function testSetStateIncomplete(array $params, string $missingKey): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('No ' . $missingKey . ' given');
        DnsResult::__set_state($params);
    }

    /**
     * Tests an answer with an invalid type
     *
     * @return void
     */
    public function testAnswerInvalidType(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('No valid type given');
        DnsAnswer::__set_state([
            'type' => 'foobar',
            'result' => 'foo',
            'ttl' => 3600,
            'priority' => null,
        ]);
    }

    /**
     * Tests an answer without a result
     *
     * @return void
     */
    public function testAnswerNoResult(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('No result given');
        DnsAnswer::__set_state([
            'type' => Dns::TXT,
            /*'result' => 'foo',*/
            'ttl' => 3600,
            'priority' => null,
        ]);
    }

    /**
     * Tests an answer without a result
     *
     * @return void
     */
    public function testAnswerNoTtl(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('No ttl given');
        DnsAnswer::__set_state([
            'type' => Dns::TXT,
            'result' => 'foo',
            /*'ttl' => 3600,*/
            'priority' => null,
        ]);
    }
}
