<?php

declare(strict_types=1);

namespace Tests\Model;

use Garrcomm\Netutils\Model\Ipv4Address;
use PHPUnit\Framework\TestCase;

/**
 * Test for the Ipv4Address model
 *
 * Normally it's not really interesting to test get and set methods, but here, calculations may apply.
 * Those calculations should be verified.
 *
 * @package App\Tests
 */
class Ipv4AddressTest extends TestCase
{
    /**
     * Test data for the IPv4 Address type
     *
     * This data provider can be used for multiple tests; each row has data that matches the complete row.
     *
     * @return array{string, string, int, string, string, string, int}[]
     */
    public function ipv4AddressTestData(): array
    {
        return array(
            // cidrAddress     ipAddress      bitmask subnetAddress     broadcastAddress  networkAddress ipRangeCount
            ['192.168.2.1/24', '192.168.2.1', 24,     '255.255.255.0',  '192.168.2.255',  '192.168.2.0', 256],
            ['10.0.10.22/24',  '10.0.10.22',  24,     '255.255.255.0',  '10.0.10.255',    '10.0.10.0',   256],
            ['172.17.0.1/16',  '172.17.0.1',  16,     '255.255.0.0',    '172.17.255.255', '172.17.0.0',  65536],
            ['10.0.0.8/8',     '10.0.0.8',    8,      '255.0.0.0',      '10.255.255.255', '10.0.0.0',    16777216],
        );
    }

    /**
     * Tests the Cidr constructor of the Ipv4Address object
     *
     * @param string  $cidrAddress      CIDR notated address.
     * @param string  $ipAddress        IPv4 address.
     * @param integer $bitmask          Bitmask.
     * @param string  $subnetAddress    Subnet address.
     * @param string  $broadcastAddress Broadcast address.
     * @param string  $networkAddress   Network address.
     * @param integer $ipRangeCount     Amount of IP addresses in range.
     *
     * @return void
     *
     * @dataProvider ipv4AddressTestData
     */
    public function testAddressByCidrConstructor(
        string $cidrAddress,
        string $ipAddress,
        int $bitmask,
        string $subnetAddress,
        string $broadcastAddress,
        string $networkAddress,
        int $ipRangeCount
    ): void {
        $address = new Ipv4Address($cidrAddress);
        $this->assertGetters(
            $address,
            $cidrAddress,
            $ipAddress,
            $bitmask,
            $subnetAddress,
            $broadcastAddress,
            $networkAddress,
            $ipRangeCount
        );
    }

    /**
     * Tests the IP+Subnet constructor of the Ipv4Address object
     *
     * @param string  $cidrAddress      CIDR notated address.
     * @param string  $ipAddress        IPv4 address.
     * @param integer $bitmask          Bitmask.
     * @param string  $subnetAddress    Subnet address.
     * @param string  $broadcastAddress Broadcast address.
     * @param string  $networkAddress   Network address.
     * @param integer $ipRangeCount     Amount of IP addresses in range.
     *
     * @return void
     *
     * @dataProvider ipv4AddressTestData
     */
    public function testAddressByIpSubnetConstructor(
        string $cidrAddress,
        string $ipAddress,
        int $bitmask,
        string $subnetAddress,
        string $broadcastAddress,
        string $networkAddress,
        int $ipRangeCount
    ): void {
        $address = new Ipv4Address($ipAddress, $subnetAddress);
        $this->assertGetters(
            $address,
            $cidrAddress,
            $ipAddress,
            $bitmask,
            $subnetAddress,
            $broadcastAddress,
            $networkAddress,
            $ipRangeCount
        );
    }

    /**
     * Tests the IP+Subnet integer constructor of the Ipv4Address object
     *
     * @param string  $cidrAddress      CIDR notated address.
     * @param string  $ipAddress        IPv4 address.
     * @param integer $bitmask          Bitmask.
     * @param string  $subnetAddress    Subnet address.
     * @param string  $broadcastAddress Broadcast address.
     * @param string  $networkAddress   Network address.
     * @param integer $ipRangeCount     Amount of IP addresses in range.
     *
     * @return void
     *
     * @dataProvider ipv4AddressTestData
     */
    public function testAddressByNumericConstructor(
        string $cidrAddress,
        string $ipAddress,
        int $bitmask,
        string $subnetAddress,
        string $broadcastAddress,
        string $networkAddress,
        int $ipRangeCount
    ): void {
        $address = new Ipv4Address(ip2long($ipAddress) ?: null, ip2long($subnetAddress) ?: null);
        $address->setIpAddress($ipAddress);
        $this->assertGetters(
            $address,
            $cidrAddress,
            $ipAddress,
            $bitmask,
            $subnetAddress,
            $broadcastAddress,
            $networkAddress,
            $ipRangeCount
        );
    }

    /**
     * Tests the setter methods of the Ipv4Address object
     *
     * @param string  $cidrAddress CIDR notated address.
     * @param string  $ipAddress   IPv4 address.
     * @param integer $bitmask     Bitmask.
     * @param string  $subnet      Subnet address.
     * @param string  $broadcast   Broadcast address.
     * @param string  $network     Network address.
     * @param integer $ipCount     Amount of IP addresses in range.
     *
     * @return void
     *
     * @dataProvider ipv4AddressTestData
     */
    public function testSetters(
        string $cidrAddress,
        string $ipAddress,
        int $bitmask,
        string $subnet,
        string $broadcast,
        string $network,
        int $ipCount
    ): void {
        // Combi IP + bitmask
        $ipBitmask = new Ipv4Address();
        $ipBitmask->setIpAddress($ipAddress)->setBitMask($bitmask);
        $this->assertGetters($ipBitmask, $cidrAddress, $ipAddress, $bitmask, $subnet, $broadcast, $network, $ipCount);

        // CIDR
        $cidr = new Ipv4Address();
        $cidr->setCidrAddress($cidrAddress);
        $this->assertGetters($cidr, $cidrAddress, $ipAddress, $bitmask, $subnet, $broadcast, $network, $ipCount);

        // Combi IP + subnet as string
        $ipSubnet = new Ipv4Address();
        $ipSubnet->setIpAddress($ipAddress)->setSubnetAddress($subnet);
        $this->assertGetters($ipSubnet, $cidrAddress, $ipAddress, $bitmask, $subnet, $broadcast, $network, $ipCount);

        // Combi IP + subnet as long
        $ipSubnetLng = new Ipv4Address();
        $ipSubnetLng->setIpAddress(ip2long($ipAddress) ?: null)->setSubnetAddress(ip2long($subnet) ?: null);
        $this->assertGetters($ipSubnetLng, $cidrAddress, $ipAddress, $bitmask, $subnet, $broadcast, $network, $ipCount);
    }

    /**
     * Test invalid value for setSubnetAddress
     *
     * @return void
     */
    public function testInvalidSubnet(): void
    {
        $ip = new Ipv4Address();
        $this->expectException(\InvalidArgumentException::class);
        $ip->setSubnetAddress('abc');
    }

    /**
     * Test no subnet value, causing no broadcast address, etcetera
     *
     * @return void
     */
    public function testNoSubnetSoNoBroadcastEtcetera(): void
    {
        $ip = new Ipv4Address('192.168.2.1');
        $ip->setSubnetAddress(null);
        $this->assertNull($ip->getSubnetAddress());
        $this->assertNull($ip->getBroadcastAddress());
        $this->assertNull($ip->getNetworkAddress());
        $this->assertNull($ip->getBitMask());
        $this->assertNull($ip->getIpRange());
    }

    /**
     * Tests the cases where IP address is null
     *
     * @return void
     */
    public function testNoIpSoNoNetworkAddressEtcetera(): void
    {
        $ip = new Ipv4Address();
        $ip->setIpAddress(null);
        $this->assertNull($ip->getIpAddress());
        $this->assertNull($ip->getBroadcastAddress());
        $this->assertNull($ip->getNetworkAddress());
        $this->assertNull($ip->getCidrAddress());
    }

    /**
     * Test invalid value for setCidrAddress (no slash)
     *
     * @return void
     */
    public function testInvalidCidrNoSlash(): void
    {
        $ip = new Ipv4Address();
        $this->expectException(\InvalidArgumentException::class);
        $ip->setCidrAddress('127.0.0.1');
    }

    /**
     * Test invalid value for setCidrAddress (invalid ip)
     *
     * @return void
     */
    public function testInvalidCidrInvalidIp(): void
    {
        $ip = new Ipv4Address();
        $this->expectException(\InvalidArgumentException::class);
        $ip->setCidrAddress('ab/c');
    }

    /**
     * Tests all the get methods of an Ipv4Address object
     *
     * @param Ipv4Address $ipv4Address      The IPv4 address object.
     * @param string      $cidrAddress      CIDR notated address.
     * @param string      $ipAddress        IPv4 address.
     * @param integer     $bitmask          Bitmask.
     * @param string      $subnetAddress    Subnet address.
     * @param string      $broadcastAddress Broadcast address.
     * @param string      $networkAddress   Network address.
     * @param integer     $ipRangeCount     Amount of IP addresses in range.
     *
     * @return void
     */
    private function assertGetters(
        Ipv4Address $ipv4Address,
        string $cidrAddress,
        string $ipAddress,
        int $bitmask,
        string $subnetAddress,
        string $broadcastAddress,
        string $networkAddress,
        int $ipRangeCount
    ): void {
        $this->assertEquals($cidrAddress, $ipv4Address->getCidrAddress(), 'CIDR address doesn\'t match');
        $this->assertEquals($ipAddress, $ipv4Address->getIpAddress(), 'IP address doesn\'t match');
        $this->assertEquals($bitmask, $ipv4Address->getBitMask(), 'Bitmask doesn\'t match');
        $this->assertEquals($subnetAddress, $ipv4Address->getSubnetAddress(), 'Subnet address doesn\'t match');
        $this->assertEquals($broadcastAddress, $ipv4Address->getBroadcastAddress(), 'Broadcast address doesn\'t match');
        $this->assertEquals($networkAddress, $ipv4Address->getNetworkAddress(), 'Network address doesn\'t match');
        $this->assertEquals($ipRangeCount, count($ipv4Address->getIpRange() ?? []), 'IP range count doesn\'t match');
        $this->assertEquals($cidrAddress, (string)$ipv4Address, '__to_string doesn\'t match');
        $this->assertJson(json_encode($ipv4Address) ?: '');
    }

    /**
     * Tests an invalid argument exception
     *
     * @return void
     */
    public function testException(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        new Ipv4Address('invalidip');
    }

    /**
     * Tests the __set_state method
     *
     * @return void
     */
    public function testSetState(): void
    {
        $ip = Ipv4Address::__set_state([
            'ipLong' => 3232235777,
            'subnetLong' => 4294967040,
        ]);
        $this->assertEquals('192.168.1.1', $ip->getIpAddress());
        $this->assertEquals('255.255.255.0', $ip->getSubnetAddress());

        $ip = Ipv4Address::__set_state([]);
        $this->assertNull($ip->getIpAddress());
        $this->assertNull($ip->getSubnetAddress());
    }

    /**
     * Tests with an invalid IP value
     *
     * @return void
     */
    public function testSetStateWrongIp(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('ipLong must be numeric');
        Ipv4Address::__set_state([
            'ipLong' => '127.0.0.1',
        ]);
    }

    /**
     * Tests with an invalid subnet value
     *
     * @return void
     */
    public function testSetStateWrongSubnet(): void
    {
        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('subnetLong must be numeric');
        Ipv4Address::__set_state([
            'ipLong' => 3232235777,
            'subnetLong' => '127.0.0.1',
        ]);
    }
}
