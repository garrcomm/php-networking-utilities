<?php

namespace Tests\Service;

use Garrcomm\Netutils\Model\Ipv4Address;
use Garrcomm\Netutils\Service\IpTools;

class IpToolsNetworkQuickScanTest extends AbstractIpToolsHelper
{
    /**
     * Successful test with Linux commands
     *
     * @return void
     */
    public function testLinux(): void
    {
        $ipTools = new IpTools('Linux');
        $this->changeCommand($ipTools, 'arp', __DIR__ . '/../iptools-sym-cmd arp');
        $this->changeCommand($ipTools, 'ping', __DIR__ . '/../iptools-sym-cmd ping');

        $network = new Ipv4Address('172.18.0.8', '255.255.0.0');
        $ips = $ipTools->networkQuickScan($network);
        $this->assertIsArray($ips);
        foreach ($ips as $ip) {
            $this->assertInstanceOf(Ipv4Address::class, $ip);
        }
    }

    /**
     * Successful test with Windows arp command (skip ping)
     *
     * @return void
     */
    public function testWindows(): void
    {
        $ipTools = $this->getIpToolsForWindowsWithoutIcmpPing();
        $this->changeCommand($ipTools, 'arp', __DIR__ . '/../iptools-sym-cmd arp');

        $network = new Ipv4Address('172.18.0.8', '255.255.0.0');
        $ips = $ipTools->networkQuickScan($network);
        $this->assertIsArray($ips);
        foreach ($ips as $ip) {
            $this->assertInstanceOf(Ipv4Address::class, $ip);
        }
    }

    /**
     * Test with Windows commands with invalid arp command
     *
     * @return void
     */
    public function testWindowsNoArp(): void
    {
        $ipTools = $this->getIpToolsForWindowsWithoutIcmpPing();
        $this->changeCommand($ipTools, 'arp', __DIR__ . '/../iptools-sym-cmd 127 arp');

        $network = new Ipv4Address('172.18.0.8', '255.255.0.0');
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionCode(127);
        $ipTools->networkQuickScan($network);
    }

    /**
     * Test with Linux commands with invalid ping command
     *
     * @return void
     */
    public function testLinuxNoPing(): void
    {
        $ipTools = new IpTools('Linux');
        $this->changeCommand($ipTools, 'arp', __DIR__ . '/../iptools-sym-cmd arp');
        $this->changeCommand($ipTools, 'ping', __DIR__ . '/../iptools-sym-cmd 127 ping');

        $network = new Ipv4Address('172.18.0.8', '255.255.0.0');
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionCode(127);
        $ipTools->networkQuickScan($network);
    }

    /**
     * Test with Windows, but native ping results in an exception
     * Because Linux can't open SOCK_RAW for non-root and unit tests run on Linux
     *
     * @return void
     */
    public function testWindowsNoPing(): void
    {
        $ipTools = new IpTools('WINNT');

        $network = new Ipv4Address('172.18.0.8', '255.255.0.0');
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionCode(0);
        $ipTools->networkQuickScan($network);
    }

    /**
     * Test with Linux commands with invalid arp command
     *
     * @return void
     */
    public function testLinuxNoArpNoIp(): void
    {
        $ipTools = new IpTools('Linux');
        $this->changeCommand($ipTools, 'arp', __DIR__ . '/../iptools-sym-cmd 127 arp');
        $this->changeCommand($ipTools, 'ping', __DIR__ . '/../iptools-sym-cmd ping');
        $this->changeCommand($ipTools, 'ip', __DIR__ . '/../iptools-sym-cmd 127 ip');

        $network = new Ipv4Address('172.18.0.8', '255.255.0.0');
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionCode(127);
        $ipTools->networkQuickScan($network);
    }
}
