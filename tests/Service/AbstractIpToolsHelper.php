<?php

namespace Tests\Service;

use Garrcomm\Netutils\Service\IpTools;
use PHPUnit\Framework\TestCase;

class AbstractIpToolsHelper extends TestCase
{
    /**
     * Changes a specific command used by IpTools
     *
     * @param IpTools $ipTools     Reference to an IpTools instance.
     * @param string  $commandName Name of the command.
     * @param string  $simulator   Command that simulates the original command.
     *
     * @return void
     */
    protected function changeCommand(IpTools $ipTools, string $commandName, string $simulator): void
    {
        $reflection = new \ReflectionClass($ipTools);
        $reflectionProperty = $reflection->getProperty('cmd' . ucfirst($commandName));
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($ipTools, $simulator);
    }

    /**
     * Returns an IpTools instance but with nativeIcmpPing disabled.
     *
     * This is done, because Linux can't open SOCK_RAW for non-root and unit tests run on Linux.
     *
     * @return IpTools
     */
    protected function getIpToolsForWindowsWithoutIcmpPing(): IpTools
    {
        return new class extends IpTools {
            /**
             * Constructor
             */
            public function __construct()
            {
                parent::__construct('WINNT');
            }
            /**
             * Always returns true
             *
             * @param string  $host            Ignored.
             * @param boolean $waitForResponse Ignored.
             *
             * @return boolean
             */
            protected function nativeIcmpPing(string $host, bool $waitForResponse = false): bool
            {
                return true;
            }
        };
    }
}
