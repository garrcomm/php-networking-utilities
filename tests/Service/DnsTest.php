<?php

namespace Tests\Service;

use Garrcomm\Netutils\Model\DnsResult;
use Garrcomm\Netutils\Service\Dns;
use RuntimeException;

class DnsTest extends AbstractDnsHelper
{
    /**
     * Returns the simulators to simulate different operating systems
     *
     * @return array<string, array{simulator: string}>
     */
    public function osDataProvider(): array
    {
        return [
            'Linux' => ['simulator' => __DIR__ . '/../nslookup-linux-sym-cmd'],
            'Windows' => ['simulator' => __DIR__ . '/../nslookup-windows-sym-cmd'],
        ];
    }

    /**
     * Tests the error "nslookup: not found"
     *
     * @param string $simulator Provided by osDataProvider; the command line simulator.
     *
     * @return void
     *
     * @dataProvider osDataProvider
     */
    public function testNotInstalled(string $simulator): void
    {
        $dns = new Dns();
        $this->changeCommand($dns, 'nslookup', $simulator . ' 127');
        $this->expectException(RuntimeException::class);
        $this->expectExceptionCode(127);
        $dns->getTxt('example.com');
    }

    /**
     * Tests the get and set DnsServer
     *
     * @return void
     */
    public function testGetSetDnsServer(): void
    {
        $dns = new Dns();
        $this->assertEquals('dns.google', $dns->getDnsServer());
        $dns->setDnsServer('8.8.4.4');
        $this->assertEquals('8.8.4.4', $dns->getDnsServer());
    }

    /**
     * Tests a non-existent record
     *
     * @param string $simulator Provided by osDataProvider; the command line simulator.
     *
     * @return void
     *
     * @dataProvider osDataProvider
     */
    public function testRecordNotFound(string $simulator): void
    {
        $dns = new Dns();
        $this->changeCommand($dns, 'nslookup', $simulator);
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessageMatches('/NXDOMAIN/');
        $this->expectExceptionCode(1);
        $dns->getTxt('nonexistent.example.com');
    }

    /**
     * Tests a successful TXT record.
     *
     * @param string $simulator Provided by osDataProvider; the command line simulator.
     *
     * @return void
     *
     * @dataProvider osDataProvider
     */
    public function testTxtSuccess(string $simulator): void
    {
        $dns = new Dns();
        $this->changeCommand($dns, 'nslookup', $simulator);
        $result = $dns->getTxt('example.com');
        $this->assertInstanceOf(DnsResult::class, $result);
        $this->assertEquals('example.com, type = TXT, class = IN', $result->getQuestion());
        $this->assertCount(2, $result->getAnswers());
        $this->assertEquals('wgyf8z8cgvm2qmxpnbnldrcltvk4xqfn', $result->getAnswers()[0]->getResult());
        $this->assertIsNumeric($result->getAnswers()[0]->getTtl());
        $this->assertNull($result->getAnswers()[0]->getPriority());
        $this->assertEquals(Dns::TXT, $result->getAnswers()[0]->getType());
        $this->assertEquals('v=spf1 -all', $result->getAnswers()[1]->getResult());
        $this->assertEquals('v=spf1 -all', (string)$result->getAnswers()[1]);
        $this->assertIsNumeric($result->getAnswers()[1]->getTtl());
        $this->assertNull($result->getAnswers()[1]->getPriority());
        $this->assertEquals(Dns::TXT, $result->getAnswers()[1]->getType());
    }

    /**
     * Tests an empty TXT record. Also tests getDnsHostname and getDnsAddress
     *
     * @param string $simulator Provided by osDataProvider; the command line simulator.
     *
     * @return void
     *
     * @dataProvider osDataProvider
     */
    public function testTxtEmpty(string $simulator): void
    {
        $dns = new Dns();
        $this->changeCommand($dns, 'nslookup', $simulator);
        $result = $dns->getTxt('empty.example.com');
        $this->assertInstanceOf(DnsResult::class, $result);
        $this->assertEquals('dns.google', $result->getDnsHostname());
        $this->assertEquals('8.8.8.8', $result->getDnsAddress());
        $this->assertEquals('empty.example.com, type = TXT, class = IN', $result->getQuestion());
        $this->assertCount(0, $result->getAnswers());
    }

    /**
     * Tests a successful NS record. Also tests jsonSerialize().
     *
     * @param string $simulator Provided by osDataProvider; the command line simulator.
     *
     * @return void
     *
     * @dataProvider osDataProvider
     */
    public function testNsSuccess(string $simulator): void
    {
        $dns = new Dns();
        $this->changeCommand($dns, 'nslookup', $simulator);
        $result = $dns->getNs('example.com');
        $this->assertInstanceOf(DnsResult::class, $result);
        $this->assertEquals('example.com, type = NS, class = IN', $result->getQuestion());
        $this->assertCount(2, $result->getAnswers());
        $this->assertEquals('b.iana-servers.net', $result->getAnswers()[0]->getResult());
        $this->assertIsNumeric($result->getAnswers()[0]->getTtl());
        $this->assertNull($result->getAnswers()[0]->getPriority());
        $this->assertEquals('a.iana-servers.net', $result->getAnswers()[1]->getResult());
        $this->assertIsNumeric($result->getAnswers()[1]->getTtl());
        $this->assertNull($result->getAnswers()[1]->getPriority());
        $this->assertEquals('{"server":"dns.google","question":"example.com, type = NS, class = IN","answers":'
            . '["b.iana-servers.net","a.iana-servers.net"]}', json_encode($result));
    }

    /**
     * Tests a successful MX record
     *
     * @param string $simulator Provided by osDataProvider; the command line simulator.
     *
     * @return void
     *
     * @dataProvider osDataProvider
     */
    public function testMxSuccess(string $simulator): void
    {
        $dns = new Dns();
        $this->changeCommand($dns, 'nslookup', $simulator);
        $result = $dns->getMx('google.com');
        $this->assertInstanceOf(DnsResult::class, $result);
        $this->assertEquals('google.com, type = MX, class = IN', $result->getQuestion());
        $this->assertCount(1, $result->getAnswers());
        $this->assertEquals('smtp.google.com', $result->getAnswers()[0]->getResult());
        $this->assertEquals('10 smtp.google.com', (string)$result->getAnswers()[0]);
        $this->assertEquals(169, $result->getAnswers()[0]->getTtl());
        $this->assertEquals(10, $result->getAnswers()[0]->getPriority());
    }

    /**
     * Tests an empty MX record
     *
     * @param string $simulator Provided by osDataProvider; the command line simulator.
     *
     * @return void
     *
     * @dataProvider osDataProvider
     */
    public function testMxEmpty(string $simulator): void
    {
        $dns = new Dns();
        $this->changeCommand($dns, 'nslookup', $simulator);
        $result = $dns->getMx('example.com');
        $this->assertInstanceOf(DnsResult::class, $result);
        $this->assertEquals('example.com, type = MX, class = IN', $result->getQuestion());
        $this->assertCount(0, $result->getAnswers());
    }

    /**
     * Tests a successful A record
     *
     * @param string $simulator Provided by osDataProvider; the command line simulator.
     *
     * @return void
     *
     * @dataProvider osDataProvider
     */
    public function testASuccess(string $simulator): void
    {
        $dns = new Dns();
        $this->changeCommand($dns, 'nslookup', $simulator);
        $result = $dns->getA('example.com');
        $this->assertInstanceOf(DnsResult::class, $result);
        $this->assertEquals('example.com, type = A, class = IN', $result->getQuestion());
        $this->assertCount(1, $result->getAnswers());
        $this->assertEquals('93.184.215.14', $result->getAnswers()[0]->getResult());
        $this->assertIsNumeric($result->getAnswers()[0]->getTtl());
        $this->assertNull($result->getAnswers()[0]->getPriority());
    }

    /**
     * Tests a successful AAAA record
     *
     * @param string $simulator Provided by osDataProvider; the command line simulator.
     *
     * @return void
     *
     * @dataProvider osDataProvider
     */
    public function testAaaaSuccess(string $simulator): void
    {
        $dns = new Dns();
        $this->changeCommand($dns, 'nslookup', $simulator);
        $result = $dns->getAAAA('example.com');
        $this->assertInstanceOf(DnsResult::class, $result);
        $this->assertEquals('example.com, type = AAAA, class = IN', $result->getQuestion());
        $this->assertCount(1, $result->getAnswers());
        $this->assertEquals('2606:2800:21f:cb07:6820:80da:af6b:8b2c', $result->getAnswers()[0]->getResult());
        $this->assertIsNumeric($result->getAnswers()[0]->getTtl());
        $this->assertNull($result->getAnswers()[0]->getPriority());
    }

    /**
     * Tests a successful CNAME record
     *
     * @param string $simulator Provided by osDataProvider; the command line simulator.
     *
     * @return void
     *
     * @dataProvider osDataProvider
     */
    public function testCnameSuccess(string $simulator): void
    {
        $dns = new Dns();
        $this->changeCommand($dns, 'nslookup', $simulator);
        $result = $dns->getCname('cname.example.com');
        $this->assertInstanceOf(DnsResult::class, $result);
        $this->assertEquals('cname.example.com, type = CNAME, class = IN', $result->getQuestion());
        $this->assertCount(1, $result->getAnswers());
        $this->assertEquals('www.example.com', $result->getAnswers()[0]->getResult());
        $this->assertIsNumeric($result->getAnswers()[0]->getTtl());
        $this->assertNull($result->getAnswers()[0]->getPriority());
    }

    /**
     * Long TXT records are separated in strings of 255 bytes, separated by a newline
     *
     * @param string $simulator Provided by osDataProvider; the command line simulator.
     *
     * @return void
     *
     * @dataProvider osDataProvider
     */
    public function testLongTxt(string $simulator): void
    {
        $expectedResult = 'v=DKIM1; k=rsa; p=TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdC'
            . 'wgc2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYSBhbGlxdWEuIFV0IGVuaW0gYW'
            . 'QgbWluaW0gdmVuaWFtLCBxdWlzIG5vc3RydWQgZXhlcmNpdGF0aW9uIHVsb' . PHP_EOL . 'GFtY28gbGFib3JpcyBuaXNpIHV0IGF'
            . 'saXF1aXAgZXggZWEgY29tbW9kbyBjb25zZXF1YXQuIER1aXMgYXV0ZSBpcnVyZSBkb2xvciBpbiByZXByZWhlbmRlcml0IGluIHZvbHV'
            . 'wdGF0ZSB2ZWxpdCBlc3Nl';

        $dns = new Dns();
        $this->changeCommand($dns, 'nslookup', $simulator);
        $result = $dns->getTxt('_dkim.example.com');
        $this->assertEquals($expectedResult, $result->getAnswers()[0]->getResult());
    }

    /**
     * Tests if the constructor works properly
     *
     * @return void
     */
    public function testConstructor(): void
    {
        $dns = new Dns('a.iana-servers.net');
        $this->assertEquals('a.iana-servers.net', $dns->getDnsServer());
    }

    /**
     * Tests with invalid result text from the console
     *
     * @return void
     */
    public function testInvalidInput(): void
    {
        $dns = new Dns();
        $this->changeCommand($dns, 'nslookup', 'ls #');
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Invalid response from `nslookup`');
        $dns->getA('example.com');
    }
}
