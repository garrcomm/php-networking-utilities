<?php

namespace Tests\Service;

use Garrcomm\Netutils\Model\Ipv4Address;
use Garrcomm\Netutils\Model\MacAddress;
use Garrcomm\Netutils\Service\IpTools;

class IpToolsGetIpByMacTest extends AbstractIpToolsHelper
{
    /**
     * Successful request using Linux
     *
     * @return void
     */
    public function testGetIpByMacLinux(): void
    {
        $ipTools = new IpTools('Linux');
        $this->changeCommand($ipTools, 'ip', __DIR__ . '/../iptools-sym-cmd ip');
        $this->changeCommand($ipTools, 'ping', __DIR__ . '/../iptools-sym-cmd ping');
        $this->changeCommand($ipTools, 'ip', __DIR__ . '/../iptools-sym-cmd ip');
        $result = $ipTools->getIpByMac(new MacAddress('02:42:7c:9a:d5:b7'));
        $this->assertInstanceOf(Ipv4Address::class, $result);
    }

    /**
     * Successful request using Windows
     *
     * @return void
     */
    public function testGetIpByMacWinNT(): void
    {
        $ipTools = $this->getIpToolsForWindowsWithoutIcmpPing();
        $this->changeCommand($ipTools, 'ipconfig', __DIR__ . '/../iptools-sym-cmd ipconfig');
        $this->changeCommand($ipTools, 'arp', __DIR__ . '/../iptools-sym-cmd arp');
        $this->changeCommand($ipTools, 'ip', __DIR__ . '/../iptools-sym-cmd ip');
        $result = $ipTools->getIpByMac(new MacAddress('02-42-ac-12-00-02'));
        $this->assertInstanceOf(Ipv4Address::class, $result);
    }

    /**
     * Successful request using Windows
     *
     * @return void
     */
    public function testGetIpByMacNotFound(): void
    {
        $ipTools = $this->getIpToolsForWindowsWithoutIcmpPing();
        $this->changeCommand($ipTools, 'ipconfig', __DIR__ . '/../iptools-sym-cmd ipconfig');
        $this->changeCommand($ipTools, 'arp', __DIR__ . '/../iptools-sym-cmd arp');
        $this->changeCommand($ipTools, 'ip', __DIR__ . '/../iptools-sym-cmd ip');
        $result = $ipTools->getIpByMac(new MacAddress('01-02-03-04-05-06'));
        $this->assertNull($result);
    }
}
