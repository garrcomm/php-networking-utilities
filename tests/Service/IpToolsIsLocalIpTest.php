<?php

namespace Tests\Service;

use Garrcomm\Netutils\Model\Ipv4Address;
use Garrcomm\Netutils\Service\IpTools;
use PHPUnit\Framework\TestCase;
use InvalidArgumentException;

class IpToolsIsLocalIpTest extends TestCase
{
    /**
     * Data provider containing a set of input strings and expected results
     *
     * @return array{input: string, output: bool}[]
     */
    public function dataProvider(): array
    {
        return [
            'Valid class A IP local address' => ['input' => '10.0.0.1',    'output' => true],
            'Valid class A IP web address'   => ['input' => '8.8.8.8',     'output' => false],
            'Valid class B IP local address' => ['input' => '172.16.0.1',  'output' => true],
            'Valid class B IP web address'   => ['input' => '172.32.0.1',  'output' => false],
            'Valid class C IP local address' => ['input' => '192.168.0.1', 'output' => true],
            'Valid class C IP web address'   => ['input' => '192.169.0.1', 'output' => false],
        ];
    }

    /**
     * Tests by using a string as input
     *
     * @param string  $input  Input value.
     * @param boolean $output Expected result.
     *
     * @return       void
     * @dataProvider dataProvider
     */
    public function testString(string $input, bool $output): void
    {
        $result = (new IpTools())->isLocalIp($input);
        if ($output) {
            $this->assertTrue($result);
        } else {
            $this->assertFalse($result);
        }
    }

    /**
     * Tests by using an Ipv4Address object as input
     *
     * @param string  $input  Input value.
     * @param boolean $output Expected result.
     *
     * @return       void
     * @dataProvider dataProvider
     */
    public function testObject(string $input, bool $output): void
    {
        $result = (new IpTools())->isLocalIp(new Ipv4Address($input));
        if ($output) {
            $this->assertTrue($result);
        } else {
            $this->assertFalse($result);
        }
    }

    /**
     * Tests by using a numeric value as input
     *
     * @param string  $input  Input value.
     * @param boolean $output Expected result.
     *
     * @return       void
     * @dataProvider dataProvider
     */
    public function testNumeric(string $input, bool $output): void
    {
        $result = (new IpTools())->isLocalIp(ip2long($input) ?: null);
        $this->assertEquals($output, $result);
    }

    /**
     * Tests the exception for invalid string input
     *
     * @return void
     */
    public function testInvalidString(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Invalid IP address specified: \'foobar\'');
        (new IpTools())->isLocalIp('foobar');
    }

    /**
     * Tests the exception for invalid object
     *
     * @return void
     */
    public function testInvalidObject(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Expected int, string or Ipv4Address. Not object');
        /* @phpstan-ignore-next-line */
        (new IpTools())->isLocalIp(new \stdClass());
    }

    /**
     * Tests the exception for an invalid integer (too high)
     *
     * @return void
     */
    public function testInvalidNumericHigh(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Expect a value from 0 to 4294967295. Not 4294967296');
        (new IpTools())->isLocalIp(4294967296);
    }

    /**
     * Tests the exception for an invalid integer (too low)
     *
     * @return void
     */
    public function testInvalidNumericLow(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $this->expectExceptionMessage('Expect a value from 0 to 4294967295. Not -1');
        (new IpTools())->isLocalIp(-1);
    }
}
