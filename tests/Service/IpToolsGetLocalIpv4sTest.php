<?php

declare(strict_types=1);

namespace Tests\Service;

use Garrcomm\Netutils\Model\Ipv4Address;
use Garrcomm\Netutils\Service\IpTools;

class IpToolsGetLocalIpv4sTest extends AbstractIpToolsHelper
{
    /**
     * Tests getLocalIpv4s with Linux / ifconfig
     *
     * @return void
     */
    public function testLinuxIfconfig(): void
    {
        $ipTools = new IpTools('Linux');
        $this->changeCommand($ipTools, 'ip', __DIR__ . '/../iptools-sym-cmd 127 ip');
        $this->changeCommand($ipTools, 'ifconfig', __DIR__ . '/../iptools-sym-cmd ifconfig');
        $ips = $ipTools->getLocalIpv4s();
        $this->assertIsArray($ips);
        foreach ($ips as $ip) {
            $this->assertInstanceOf(Ipv4Address::class, $ip);
        }
    }

    /**
     * Tests getLocalIpv4s with Windows / ipconfig
     *
     * @return void
     */
    public function testWindowsIpconfig(): void
    {
        $ipTools = new IpTools('WINNT');
        $this->changeCommand($ipTools, 'ipconfig', __DIR__ . '/../iptools-sym-cmd ipconfig');
        $ips = $ipTools->getLocalIpv4s();
        $this->assertIsArray($ips);
        foreach ($ips as $ip) {
            $this->assertInstanceOf(Ipv4Address::class, $ip);
        }
    }

    /**
     * Tests getLocalIpv4s with Linux / ip
     *
     * @return void
     */
    public function testLinuxIp(): void
    {
        $ipTools = new IpTools('Linux');
        $this->changeCommand($ipTools, 'ip', __DIR__ . '/../iptools-sym-cmd ip');
        $this->changeCommand($ipTools, 'ifconfig', __DIR__ . '/../iptools-sym-cmd 127 ifconfig');
        $ips = $ipTools->getLocalIpv4s();
        $this->assertIsArray($ips);
        foreach ($ips as $ip) {
            $this->assertInstanceOf(Ipv4Address::class, $ip);
        }
    }

    /**
     * Tests getLocalIpv4s with Linux / no available tools
     *
     * @return void
     */
    public function testLinuxNone(): void
    {
        $ipTools = new IpTools('Linux');
        $this->changeCommand($ipTools, 'ip', __DIR__ . '/../iptools-iptools-sym-cmd 127 ip');
        $this->changeCommand($ipTools, 'ifconfig', __DIR__ . '/../iptools-iptools-sym-cmd 127 ifconfig');
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionCode(127);
        $ipTools->getLocalIpv4s();
    }

    /**
     * Tests getLocalIpv4s with Linux / ip addr gives an error
     *
     * @return void
     */
    public function testLinuxIpError(): void
    {
        $ipTools = new IpTools('Linux');
        $this->changeCommand($ipTools, 'ip', __DIR__ . '/../iptools-sym-cmd 1 ip');
        $this->changeCommand($ipTools, 'ifconfig', __DIR__ . '/../iptools-sym-cmd 127 ifconfig');
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionCode(1);
        $ipTools->getLocalIpv4s();
    }

    /**
     * Tests getLocalIpv4s with Linux / ifconfig gives an error
     *
     * @return void
     */
    public function testLinuxIfconfigError(): void
    {
        $ipTools = new IpTools('Linux');
        $this->changeCommand($ipTools, 'ip', __DIR__ . '/../iptools-sym-cmd 127 ip');
        $this->changeCommand($ipTools, 'ifconfig', __DIR__ . '/../iptools-sym-cmd 1 ifconfig');
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionCode(1);
        $ipTools->getLocalIpv4s();
    }

    /**
     * Tests getLocalIpv4s with Windows / ipconfig gives an error
     *
     * @return void
     */
    public function testWindowsIpconfigError(): void
    {
        $ipTools = new IpTools('WINNT');
        $this->changeCommand($ipTools, 'ipconfig', __DIR__ . '/../iptools-sym-cmd 1 ipconfig');
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionCode(1);
        $ipTools->getLocalIpv4s();
    }
}
