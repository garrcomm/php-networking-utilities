<?php

namespace Tests\Service;

use Garrcomm\Netutils\Model\Ipv4Address;
use Garrcomm\Netutils\Model\MacAddress;
use Garrcomm\Netutils\Service\IpTools;

class IpToolsGetMacByIpTest extends AbstractIpToolsHelper
{
    /**
     * Tests a successful call on Windows
     *
     * @return void
     */
    public function testSuccessWindows(): void
    {
        $ipTools = $this->getIpToolsForWindowsWithoutIcmpPing();
        $this->changeCommand($ipTools, 'arp', __DIR__ . '/../iptools-sym-cmd arp');
        $mac = $ipTools->getMacByIp(new Ipv4Address('192.168.20.193', '255.255.255.0'));
        $this->assertInstanceOf(MacAddress::class, $mac);
    }

    /**
     * Tests a successful call on Linux
     *
     * @return void
     */
    public function testSuccessLinux(): void
    {
        $ipTools = new IpTools('Linux');
        $this->changeCommand($ipTools, 'arp', __DIR__ . '/../iptools-sym-cmd arp');
        $this->changeCommand($ipTools, 'ping', __DIR__ . '/../iptools-sym-cmd ping');
        $this->changeCommand($ipTools, 'ip', __DIR__ . '/../iptools-sym-cmd ip');
        $mac = $ipTools->getMacByIp(new Ipv4Address('172.18.0.4', '255.255.0.0'));
        $this->assertInstanceOf(MacAddress::class, $mac);
    }

    /**
     * Tests a ping error call on Linux
     *
     * @return void
     */
    public function testPingErrorLinux(): void
    {
        $ipTools = new IpTools('Linux');
        $this->changeCommand($ipTools, 'arp', __DIR__ . '/../iptools-sym-cmd arp');
        $this->changeCommand($ipTools, 'ping', __DIR__ . '/../iptools-sym-cmd 127 ping');
        $this->expectException(\RuntimeException::class);
        $this->expectExceptionCode(127);
        $ipTools->getMacByIp(new Ipv4Address('172.18.0.4', '255.255.0.0'));
    }

    /**
     * Tests a non-existent call on Linux
     *
     * @return void
     */
    public function testNonExistentLinux(): void
    {
        $ipTools = new IpTools('Linux');
        $this->changeCommand($ipTools, 'arp', __DIR__ . '/../iptools-sym-cmd arp');
        $this->changeCommand($ipTools, 'ping', __DIR__ . '/../iptools-sym-cmd ping');
        $this->changeCommand($ipTools, 'ip', __DIR__ . '/../iptools-sym-cmd ip');
        $mac = $ipTools->getMacByIp(new Ipv4Address('172.18.0.32', '255.255.0.0'));
        $this->assertNull($mac);
    }

    /**
     * When the IP is null, the mac is null as well
     *
     * @return void
     */
    public function testNullGivesNull(): void
    {
        $ipv4Address = new Ipv4Address();
        $ipTools = new IpTools('Linux');
        $this->assertNull($ipTools->getMacByIp($ipv4Address));
    }
}
