<?php

namespace Tests\Service;

use Garrcomm\Netutils\Service\Dns;
use PHPUnit\Framework\TestCase;

abstract class AbstractDnsHelper extends TestCase
{
    /**
     * Changes a specific command used by Dns
     *
     * @param Dns    $dns         Reference to an IpTools instance.
     * @param string $commandName Name of the command.
     * @param string $simulator   Command that simulates the original command.
     *
     * @return void
     */
    protected function changeCommand(Dns $dns, string $commandName, string $simulator): void
    {
        $reflection = new \ReflectionClass($dns);
        $reflectionProperty = $reflection->getProperty('cmd' . ucfirst($commandName));
        $reflectionProperty->setAccessible(true);
        $reflectionProperty->setValue($dns, $simulator);
    }
}
