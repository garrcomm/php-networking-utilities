# Model\MacAddress

## Class synopsis

```php
class MacAddress implements JsonSerializable {

    /* Constants */
    public const LOWERCASE = 0;
    public const UPPERCASE = 1;

    /* Methods */
    public __construct(string macAddress)
    public format(string separator = ':', integer casing = 0): string
    public jsonSerialize(): mixed
    public set(string macAddress): MacAddress
    public __toString(): string
    public static __set_state(mixed[] state): self
}
```

<a id="constants"></a>
## Constants

**MacAddress::LOWERCASE**  
&nbsp;&nbsp;&nbsp;&nbsp;

**MacAddress::UPPERCASE**  
&nbsp;&nbsp;&nbsp;&nbsp;


<a id="methods"></a>
## Methods

* [MacAddress::__construct](#macaddress----construct) — Initializes a new MAC address model.
* [MacAddress::format](#macaddress--format) — Get a string representation of the MAC address
* [MacAddress::jsonSerialize](#macaddress--jsonserialize) — JsonSerializable::jsonSerialize — Specify data which should be serialized to JSON
* [MacAddress::set](#macaddress--set) — Set a new value
* [MacAddress::__toString](#macaddress----tostring) — Gets a string representation of the object
* [MacAddress::__set_state](#macaddress----set-state) — This static method is called for classes exported by `var_export()`.


<a id="macaddress----construct"></a>
## MacAddress::__construct
#### Description

```php
public MacAddress::__construct(string macAddress)
```

Initializes a new MAC address model.

#### Parameters

* **macAddress**  
    The MAC address.

***
<a id="macaddress--format"></a>
## MacAddress::format
#### Description

```php
public MacAddress::format(string separator = ':', integer casing = 0): string
```

Get a string representation of the MAC address

#### Parameters

* **separator**  
    Default ':', but can also be '-'.
* **casing**  
    Should be self:LOWERCASE or self:UPPERCASE.

#### Return Values



***
<a id="macaddress--jsonserialize"></a>
## MacAddress::jsonSerialize
#### Description

```php
public MacAddress::jsonSerialize(): mixed
```

JsonSerializable::jsonSerialize — Specify data which should be serialized to JSON

#### Return Values

Returns data which can be serialized by `json_encode()`

***
<a id="macaddress--set"></a>
## MacAddress::set
#### Description

```php
public MacAddress::set(string macAddress): MacAddress
```

Set a new value

#### Parameters

* **macAddress**  
    The new value.

#### Return Values




***
<a id="macaddress----tostring"></a>
## MacAddress::__toString
#### Description

```php
public MacAddress::__toString(): string
```

Gets a string representation of the object

#### Return Values

The string representation of the object.

***
<a id="macaddress----set-state"></a>
## MacAddress::__set_state
#### Description

```php
public static MacAddress::__set_state(mixed[] state): self
```

This static method is called for classes exported by `var_export()`.

#### Parameters

* **state**  
    An array containing exported properties in the form `['property' => value, ...]`.

#### Return Values

The exported object.

#### Errors/Exceptions

Thrown when the state is incomplete.

