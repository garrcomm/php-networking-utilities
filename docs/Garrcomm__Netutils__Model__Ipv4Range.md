# Model\Ipv4Range

## Class example

You can use this object as an array:

```php
// Defines a range
$range = new \Garrcomm\Netutils\Model\Ipv4Range('192.168.2.0', 24);

// Uses the Countable interface to return the amount of IPs in range
echo 'Amount of IPs in the range: ' . count($range) . PHP_EOL;

// Uses the Iterator interface to walk through the results
foreach ($range as $ip) {
    echo '- ' . $ip . PHP_EOL;
}

// Uses the ArrayAccess interface to walk through the results
for ($i = 0; $i < count($range); ++$i) {
    echo '- ' . $range[$i] . PHP_EOL;
}

// Returns a json array with all IPs in range
echo json_encode($range, JSON_PRETTY_PRINT);

// Returns the IP range as string
echo $range . PHP_EOL;
```

## Class synopsis

```php
class Ipv4Range implements Iterator, Countable, ArrayAccess, JsonSerializable, Traversable {

    /* Methods */
    public __construct(string|integer networkAddress, integer bitMask)
    public count(): int
    public current(): mixed
    public jsonSerialize(): mixed
    public key(): mixed
    public next(): void
    public offsetExists(mixed offset): bool
    public offsetGet(mixed offset): mixed
    public offsetSet(mixed offset, mixed value): void
    public offsetUnset(mixed offset): void
    public rewind(): void
    public valid(): bool
    public __toString(): string
    public static __set_state(mixed[] state): self
}
```

<a id="methods"></a>
## Methods

* [Ipv4Range::__construct](#ipv4range----construct) — Constructs an IPv4 range
* [Ipv4Range::count](#ipv4range--count) — Countable::count — Count elements of an object
* [Ipv4Range::current](#ipv4range--current) — Iterator::current — Return the current element
* [Ipv4Range::jsonSerialize](#ipv4range--jsonserialize) — JsonSerializable::jsonSerialize — Specify data which should be serialized to JSON
* [Ipv4Range::key](#ipv4range--key) — Iterator::key — Return the key of the current element
* [Ipv4Range::next](#ipv4range--next) — Iterator::next — Move forward to next element
* [Ipv4Range::offsetExists](#ipv4range--offsetexists) — ArrayAccess::offsetExists — Whether an offset exists
* [Ipv4Range::offsetGet](#ipv4range--offsetget) — ArrayAccess::offsetGet — Offset to retrieve
* [Ipv4Range::offsetSet](#ipv4range--offsetset) — ArrayAccess::offsetSet — Assign a value to the specified offset
* [Ipv4Range::offsetUnset](#ipv4range--offsetunset) — ArrayAccess::offsetUnset — Unset an offset
* [Ipv4Range::rewind](#ipv4range--rewind) — Iterator::rewind — Rewind the Iterator to the first element
* [Ipv4Range::valid](#ipv4range--valid) — Iterator::valid — Checks if current position is valid
* [Ipv4Range::__toString](#ipv4range----tostring) — Gets a string representation of the object
* [Ipv4Range::__set_state](#ipv4range----set-state) — This static method is called for classes exported by `var_export()`.


<a id="ipv4range----construct"></a>
## Ipv4Range::__construct
#### Description

```php
public Ipv4Range::__construct(string|integer networkAddress, integer bitMask)
```

Constructs an IPv4 range

#### Parameters

* **networkAddress**  
    The network address.
* **bitMask**  
    The bit mask.

#### Errors/Exceptions

Thrown when the arguments are no valid IP addresses.

***
<a id="ipv4range--count"></a>
## Ipv4Range::count
#### Description

```php
public Ipv4Range::count(): int
```

Countable::count — Count elements of an object

#### Return Values

The custom count as an int.

***
<a id="ipv4range--current"></a>
## Ipv4Range::current
#### Description

```php
public Ipv4Range::current(): mixed
```

Iterator::current — Return the current element

#### Return Values

Can return any type.

***
<a id="ipv4range--jsonserialize"></a>
## Ipv4Range::jsonSerialize
#### Description

```php
public Ipv4Range::jsonSerialize(): mixed
```

JsonSerializable::jsonSerialize — Specify data which should be serialized to JSON

#### Return Values

Returns data which can be serialized by `json_encode()`

***
<a id="ipv4range--key"></a>
## Ipv4Range::key
#### Description

```php
public Ipv4Range::key(): mixed
```

Iterator::key — Return the key of the current element

#### Return Values

Returns scalar on success, or null on failure.

***
<a id="ipv4range--next"></a>
## Ipv4Range::next
#### Description

```php
public Ipv4Range::next(): void
```

Iterator::next — Move forward to next element

#### Return Values



***
<a id="ipv4range--offsetexists"></a>
## Ipv4Range::offsetExists
#### Description

```php
public Ipv4Range::offsetExists(mixed offset): bool
```

ArrayAccess::offsetExists — Whether an offset exists

#### Parameters

* **offset**  
    An offset to check for.

#### Return Values

Returns true on success or false on failure.

***
<a id="ipv4range--offsetget"></a>
## Ipv4Range::offsetGet
#### Description

```php
public Ipv4Range::offsetGet(mixed offset): mixed
```

ArrayAccess::offsetGet — Offset to retrieve

#### Parameters

* **offset**  
    The offset to retrieve.

#### Return Values

Can return all value types.

***
<a id="ipv4range--offsetset"></a>
## Ipv4Range::offsetSet
#### Description

```php
public Ipv4Range::offsetSet(mixed offset, mixed value): void
```

ArrayAccess::offsetSet — Assign a value to the specified offset

#### Parameters

* **offset**  
    The offset to assign the value to.
* **value**  
    The value to set.

#### Return Values



#### Errors/Exceptions

Thrown because this object is read only.

***
<a id="ipv4range--offsetunset"></a>
## Ipv4Range::offsetUnset
#### Description

```php
public Ipv4Range::offsetUnset(mixed offset): void
```

ArrayAccess::offsetUnset — Unset an offset

#### Parameters

* **offset**  
    The offset to unset.

#### Return Values



#### Errors/Exceptions

Thrown because this object is read only.

***
<a id="ipv4range--rewind"></a>
## Ipv4Range::rewind
#### Description

```php
public Ipv4Range::rewind(): void
```

Iterator::rewind — Rewind the Iterator to the first element

#### Return Values



***
<a id="ipv4range--valid"></a>
## Ipv4Range::valid
#### Description

```php
public Ipv4Range::valid(): bool
```

Iterator::valid — Checks if current position is valid

#### Return Values

The return value will be casted to bool and then evaluated.

***
<a id="ipv4range----tostring"></a>
## Ipv4Range::__toString
#### Description

```php
public Ipv4Range::__toString(): string
```

Gets a string representation of the object

#### Return Values

The string representation of the object.

***
<a id="ipv4range----set-state"></a>
## Ipv4Range::__set_state
#### Description

```php
public static Ipv4Range::__set_state(mixed[] state): self
```

This static method is called for classes exported by `var_export()`.

#### Parameters

* **state**  
    An array containing exported properties in the form `['property' => value, ...]`.

#### Return Values

The exported object.

#### Errors/Exceptions

Thrown when the state is incomplete.

