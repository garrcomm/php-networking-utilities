# Service\Dns

## Class example

The Dns service wraps around `nslookup` and returns neat DNS lookup results.
It's also possible to change the target nameserver.

```php
$dns = new \Garrcomm\Netutils\Service\Dns();
$dns->setDnsServer('dns.google');
var_dump($dns->getMx('google.com'));
```

## Class synopsis

```php
class Dns {

    /* Constants */
    public const A = 'internet address';
    public const AAAA = 'AAAA address';
    public const CNAME = 'canonical name';
    public const MX = 'mail exchanger';
    public const NS = 'nameserver';
    public const TXT = 'text';

    /* Methods */
    public __construct(string dnsServer = 'dns.google')
    public getA(string domain): DnsResult
    public getAAAA(string domain): DnsResult
    public getCname(string domain): DnsResult
    public getDnsServer(): string
    public getMx(string domain): DnsResult
    public getNs(string domain): DnsResult
    public getTxt(string domain): DnsResult
    public setDnsServer(string dnsServer): void
}
```

<a id="constants"></a>
## Constants

**Dns::A**  
&nbsp;&nbsp;&nbsp;&nbsp;An IPv4 internet address (A record)

**Dns::AAAA**  
&nbsp;&nbsp;&nbsp;&nbsp;An IPv6 internet address (AAAA record)

**Dns::CNAME**  
&nbsp;&nbsp;&nbsp;&nbsp;An alias to another DNS record (CNAME record)

**Dns::MX**  
&nbsp;&nbsp;&nbsp;&nbsp;IP address(es) to an email server (MX record)

**Dns::NS**  
&nbsp;&nbsp;&nbsp;&nbsp;DNS names of all responsible nameservers (NS record)

**Dns::TXT**  
&nbsp;&nbsp;&nbsp;&nbsp;A record containing plain text, used for domain verification, SPF, DKIM, etc. (TXT record)


<a id="methods"></a>
## Methods

* [Dns::__construct](#dns----construct) — Initiates a new DNS service
* [Dns::getA](#dns--geta) — Fetches all A records
* [Dns::getAAAA](#dns--getaaaa) — Fetches all AAAA records
* [Dns::getCname](#dns--getcname) — Fetches all CNAME records
* [Dns::getDnsServer](#dns--getdnsserver) — Returns the DNS server used to query all requests
* [Dns::getMx](#dns--getmx) — Fetches all MX records
* [Dns::getNs](#dns--getns) — Fetches all NS records
* [Dns::getTxt](#dns--gettxt) — Fetches all TXT records
* [Dns::setDnsServer](#dns--setdnsserver) — Sets the DNS server used to query all requests


<a id="dns----construct"></a>
## Dns::__construct
#### Description

```php
public Dns::__construct(string dnsServer = 'dns.google')
```

Initiates a new DNS service

#### Parameters

* **dnsServer**  
    The name server to query.

***
<a id="dns--geta"></a>
## Dns::getA
#### Description

```php
public Dns::getA(string domain): DnsResult
```

Fetches all A records

#### Parameters

* **domain**  
    Hostname to get the A records from.

#### Return Values

The A record.


***
<a id="dns--getaaaa"></a>
## Dns::getAAAA
#### Description

```php
public Dns::getAAAA(string domain): DnsResult
```

Fetches all AAAA records

#### Parameters

* **domain**  
    Hostname to get the AAAA records from.

#### Return Values

The AAAA record.


***
<a id="dns--getcname"></a>
## Dns::getCname
#### Description

```php
public Dns::getCname(string domain): DnsResult
```

Fetches all CNAME records

#### Parameters

* **domain**  
    Hostname to get the CNAME records from.

#### Return Values

The CNAME record.


***
<a id="dns--getdnsserver"></a>
## Dns::getDnsServer
#### Description

```php
public Dns::getDnsServer(): string
```

Returns the DNS server used to query all requests

#### Return Values

The current DNS server.

***
<a id="dns--getmx"></a>
## Dns::getMx
#### Description

```php
public Dns::getMx(string domain): DnsResult
```

Fetches all MX records

#### Parameters

* **domain**  
    Hostname to get the MX records from.

#### Return Values

The MX record.


***
<a id="dns--getns"></a>
## Dns::getNs
#### Description

```php
public Dns::getNs(string domain): DnsResult
```

Fetches all NS records

#### Parameters

* **domain**  
    Hostname to get the NS records from.

#### Return Values

The NS record.


***
<a id="dns--gettxt"></a>
## Dns::getTxt
#### Description

```php
public Dns::getTxt(string domain): DnsResult
```

Fetches all TXT records

#### Parameters

* **domain**  
    Hostname to get the TXT records from.

#### Return Values

The TXT record.


***
<a id="dns--setdnsserver"></a>
## Dns::setDnsServer
#### Description

```php
public Dns::setDnsServer(string dnsServer): void
```

Sets the DNS server used to query all requests

#### Parameters

* **dnsServer**  
    The DNS server.

#### Return Values



