# Model\DnsResult

## Class synopsis

```php
class DnsResult implements JsonSerializable {

    /* Methods */
    public __construct(string question, DnsAnswer[] answers, string dnsHostname, string dnsAddress)
    public getAnswers(boolean sorted = false): DnsAnswer[]
    public getDnsAddress(): string
    public getDnsHostname(): string
    public getQuestion(): string
    public jsonSerialize(): mixed
    public __toString(): string
    public static __set_state(mixed[] state): self
}
```

<a id="methods"></a>
## Methods

* [DnsResult::__construct](#dnsresult----construct) — Constructs a DNS result
* [DnsResult::getAnswers](#dnsresult--getanswers) — Returns all given answers
* [DnsResult::getDnsAddress](#dnsresult--getdnsaddress) — The address of the DNS server.
* [DnsResult::getDnsHostname](#dnsresult--getdnshostname) — Returns the hostname of the DNS server.
* [DnsResult::getQuestion](#dnsresult--getquestion) — Returns the original DNS question
* [DnsResult::jsonSerialize](#dnsresult--jsonserialize) — JsonSerializable::jsonSerialize — Specify data which should be serialized to JSON
* [DnsResult::__toString](#dnsresult----tostring) — Gets a string representation of the object
* [DnsResult::__set_state](#dnsresult----set-state) — This static method is called for classes exported by `var_export()`.


<a id="dnsresult----construct"></a>
## DnsResult::__construct
#### Description

```php
public DnsResult::__construct(string question, DnsAnswer[] answers, string dnsHostname, string dnsAddress)
```

Constructs a DNS result

#### Parameters

* **question**  
    The original question.
* **answers**  
    All DNS answers.
* **dnsHostname**  
    The hostname of the DNS server.
* **dnsAddress**  
    The address of the DNS server.

***
<a id="dnsresult--getanswers"></a>
## DnsResult::getAnswers
#### Description

```php
public DnsResult::getAnswers(boolean sorted = false): DnsAnswer[]
```

Returns all given answers

#### Parameters

* **sorted**  
    Set to true to sort the answers. Useful to sort MX records by priority for example.

#### Return Values

A list of all answers.


***
<a id="dnsresult--getdnsaddress"></a>
## DnsResult::getDnsAddress
#### Description

```php
public DnsResult::getDnsAddress(): string
```

The address of the DNS server.

#### Return Values

The DNS IP address.

***
<a id="dnsresult--getdnshostname"></a>
## DnsResult::getDnsHostname
#### Description

```php
public DnsResult::getDnsHostname(): string
```

Returns the hostname of the DNS server.

#### Return Values

The DNS hostname.

***
<a id="dnsresult--getquestion"></a>
## DnsResult::getQuestion
#### Description

```php
public DnsResult::getQuestion(): string
```

Returns the original DNS question

#### Return Values

The original question.

***
<a id="dnsresult--jsonserialize"></a>
## DnsResult::jsonSerialize
#### Description

```php
public DnsResult::jsonSerialize(): mixed
```

JsonSerializable::jsonSerialize — Specify data which should be serialized to JSON

#### Return Values

Returns data which can be serialized by `json_encode()`

***
<a id="dnsresult----tostring"></a>
## DnsResult::__toString
#### Description

```php
public DnsResult::__toString(): string
```

Gets a string representation of the object

#### Return Values

The string representation of the object.

***
<a id="dnsresult----set-state"></a>
## DnsResult::__set_state
#### Description

```php
public static DnsResult::__set_state(mixed[] state): self
```

This static method is called for classes exported by `var_export()`.

#### Parameters

* **state**  
    An array containing exported properties in the form `['property' => value, ...]`.

#### Return Values

The exported object.

#### Errors/Exceptions

Thrown when an argument is missing.

