# Model\Ipv4Address

## Class example

The `Ipv4Address` model is also used in the `IpTools` service, but it can also help with different notations
and even calculate complete subnets;

```php
$ip = new \Garrcomm\Netutils\Model\Ipv4Address('192.168.1.1', '255.255.255.0');
echo 'CIDR notation:     ' . $ip->getCidrAddress() . PHP_EOL;
echo 'Broadcast address: ' . $ip->getBroadcastAddress() . PHP_EOL;
echo 'Network address:   ' . $ip->getNetworkAddress() . PHP_EOL;
echo 'Amount of IP\'s in range: ' . count($ip->getIpRange()) . PHP_EOL;
```

## Class synopsis

```php
class Ipv4Address implements JsonSerializable {

    /* Methods */
    public __construct(?string|integer ipAddress = null, ?string|integer subnetAddress = null)
    public getBitMask(): int
    public getBroadcastAddress(): string
    public getCidrAddress(): string
    public getIpAddress(): string
    public getIpRange(): Ipv4Address[]|Ipv4Range
    public getNetworkAddress(): string
    public getSubnetAddress(): string
    public jsonSerialize(): mixed
    public setBitMask(integer bitMask): Ipv4Address
    public setCidrAddress(string ipMask): Ipv4Address
    public setIpAddress(?string|integer ipAddress): Ipv4Address
    public setSubnetAddress(?string|integer subnetAddress): Ipv4Address
    public __toString(): string
    public static __set_state(mixed[] state): self
}
```

<a id="methods"></a>
## Methods

* [Ipv4Address::__construct](#ipv4address----construct) — Creates a new IP entity
    
    Input can be an IP address as integer, regular, or in CIDR format.
    
* [Ipv4Address::getBitMask](#ipv4address--getbitmask) — Returns the bitmask presentation of the subnet
* [Ipv4Address::getBroadcastAddress](#ipv4address--getbroadcastaddress) — Returns the broadcast address
* [Ipv4Address::getCidrAddress](#ipv4address--getcidraddress) — Returns the IP address in CIDR notation
* [Ipv4Address::getIpAddress](#ipv4address--getipaddress) — Returns the IP address
* [Ipv4Address::getIpRange](#ipv4address--getiprange) — Returns the full IP range in this network
* [Ipv4Address::getNetworkAddress](#ipv4address--getnetworkaddress) — Returns the network address
* [Ipv4Address::getSubnetAddress](#ipv4address--getsubnetaddress) — Returns the subnet address
* [Ipv4Address::jsonSerialize](#ipv4address--jsonserialize) — JsonSerializable::jsonSerialize — Specify data which should be serialized to JSON
* [Ipv4Address::setBitMask](#ipv4address--setbitmask) — Sets the bitmask presentation of the subnet
* [Ipv4Address::setCidrAddress](#ipv4address--setcidraddress) — Fills this entity with an IP address based on CIDR notation
* [Ipv4Address::setIpAddress](#ipv4address--setipaddress) — Sets the IP address
* [Ipv4Address::setSubnetAddress](#ipv4address--setsubnetaddress) — Sets the subnet address
* [Ipv4Address::__toString](#ipv4address----tostring) — Gets a string representation of the object
* [Ipv4Address::__set_state](#ipv4address----set-state) — This static method is called for classes exported by `var_export()`.


<a id="ipv4address----construct"></a>
## Ipv4Address::__construct
#### Description

```php
public Ipv4Address::__construct(?string|integer ipAddress = null, ?string|integer subnetAddress = null)
```

Creates a new IP entity

 Input can be an IP address as integer, regular, or in CIDR format.

#### Parameters

* **ipAddress**  
    IP address, different formats are supported.
* **subnetAddress**  
    Subnet address, different formats are supported.

#### Errors/Exceptions

An exception will be thrown when the IP address is invalid.

***
<a id="ipv4address--getbitmask"></a>
## Ipv4Address::getBitMask
#### Description

```php
public Ipv4Address::getBitMask(): int
```

Returns the bitmask presentation of the subnet

#### Return Values

The bitmask.

***
<a id="ipv4address--getbroadcastaddress"></a>
## Ipv4Address::getBroadcastAddress
#### Description

```php
public Ipv4Address::getBroadcastAddress(): string
```

Returns the broadcast address

#### Return Values

The broadcast address.

***
<a id="ipv4address--getcidraddress"></a>
## Ipv4Address::getCidrAddress
#### Description

```php
public Ipv4Address::getCidrAddress(): string
```

Returns the IP address in CIDR notation

#### Return Values

The IP address in CIDR notation or null when there's no IP.

***
<a id="ipv4address--getipaddress"></a>
## Ipv4Address::getIpAddress
#### Description

```php
public Ipv4Address::getIpAddress(): string
```

Returns the IP address

#### Return Values

The IP address, or null when it's empty.

***
<a id="ipv4address--getiprange"></a>
## Ipv4Address::getIpRange
#### Description

```php
public Ipv4Address::getIpRange(): Ipv4Address[]|Ipv4Range
```

Returns the full IP range in this network

#### Return Values

The list of IP addresses in range.


***
<a id="ipv4address--getnetworkaddress"></a>
## Ipv4Address::getNetworkAddress
#### Description

```php
public Ipv4Address::getNetworkAddress(): string
```

Returns the network address

#### Return Values

The network address.

***
<a id="ipv4address--getsubnetaddress"></a>
## Ipv4Address::getSubnetAddress
#### Description

```php
public Ipv4Address::getSubnetAddress(): string
```

Returns the subnet address

#### Return Values

The subnet address.

***
<a id="ipv4address--jsonserialize"></a>
## Ipv4Address::jsonSerialize
#### Description

```php
public Ipv4Address::jsonSerialize(): mixed
```

JsonSerializable::jsonSerialize — Specify data which should be serialized to JSON

#### Return Values

Returns data which can be serialized by `json_encode()`

***
<a id="ipv4address--setbitmask"></a>
## Ipv4Address::setBitMask
#### Description

```php
public Ipv4Address::setBitMask(integer bitMask): Ipv4Address
```

Sets the bitmask presentation of the subnet

#### Parameters

* **bitMask**  
    The bitmask.

#### Return Values

The Ipv4Address object.


***
<a id="ipv4address--setcidraddress"></a>
## Ipv4Address::setCidrAddress
#### Description

```php
public Ipv4Address::setCidrAddress(string ipMask): Ipv4Address
```

Fills this entity with an IP address based on CIDR notation

#### Parameters

* **ipMask**  
    The IP address in CIDR notation (127.0.0.1/8).

#### Return Values

The Ipv4Address object.


#### Errors/Exceptions

An exception will be thrown when the IP address is invalid.

***
<a id="ipv4address--setipaddress"></a>
## Ipv4Address::setIpAddress
#### Description

```php
public Ipv4Address::setIpAddress(?string|integer ipAddress): Ipv4Address
```

Sets the IP address

#### Parameters

* **ipAddress**  
    The IP address as long or in dotted notation.

#### Return Values

The Ipv4Address object.


#### Errors/Exceptions

Throws an exception when the input is invalid.

***
<a id="ipv4address--setsubnetaddress"></a>
## Ipv4Address::setSubnetAddress
#### Description

```php
public Ipv4Address::setSubnetAddress(?string|integer subnetAddress): Ipv4Address
```

Sets the subnet address

#### Parameters

* **subnetAddress**  
    The subnet address as long or in dotted notation.

#### Return Values

The Ipv4Address object.


#### Errors/Exceptions

Throws an exception when the input is invalid.

***
<a id="ipv4address----tostring"></a>
## Ipv4Address::__toString
#### Description

```php
public Ipv4Address::__toString(): string
```

Gets a string representation of the object

#### Return Values

The string representation of the object.

***
<a id="ipv4address----set-state"></a>
## Ipv4Address::__set_state
#### Description

```php
public static Ipv4Address::__set_state(mixed[] state): self
```

This static method is called for classes exported by `var_export()`.

#### Parameters

* **state**  
    An array containing exported properties in the form `['property' => value, ...]`.

#### Return Values

The exported object.

