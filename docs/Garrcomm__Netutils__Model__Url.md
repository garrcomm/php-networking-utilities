# Model\Url

## Class example

This class wraps and enhances the PHP method `parse_url` so you can read and modify URLs

```php
// You can initiate the URL class in two ways;
// 1. By using the current request
$url = \Garrcomm\Netutils\Model\Url::current();
// 2. By using a URL in the constructor
$url = new \Garrcomm\Netutils\Model\Url('https://localhost/foo?one=two&bar=baz');

// It's possible to set and remove elements, in one chain.
echo $url
    ->setHostname('foo.bar')
    ->setPath('baz')
    ->setQuery('bar', 'foo')
    ->removeQuery('one')
    ->setFragment('fragment-here'); // https://foo.bar/baz?bar=foo#fragment-here

// The object can be treated as a string:
echo '<a href="' .htmlspecialchars($url) . '">Click here</a>';

// The object can also be json serialized:
echo json_encode($url);
```

## Class synopsis

```php
class Url implements JsonSerializable {

    /* Methods */
    public __construct(string url)
    public getFragment(): string
    public getFullQuery(): array<int|string,mixed>
    public getHostname(): string
    public getPassword(): string
    public getPath(): string
    public getPort(): int
    public getQuery(string key): mixed|string
    public getScheme(): string
    public getUsername(): string
    public jsonSerialize(): mixed
    public removeFragment(): self
    public removeFullQuery(): self
    public removePassword(): self
    public removePort(): self
    public removeQuery(string key): self
    public removeUsername(): self
    public setFragment(string fragment): self
    public setFullQuery(array<string,mixed> query): self
    public setHostname(string hostname): self
    public setPassword(string password): self
    public setPath(string path): self
    public setPort(integer port): self
    public setQuery(string key, string|mixed value): self
    public setUsername(string username): self
    public __toString(): string
    public static current(): self
    public static __set_state(mixed[] state): self
}
```

<a id="methods"></a>
## Methods

* [Url::__construct](#url----construct) — Makes an object based on a URL
* [Url::getFragment](#url--getfragment) — Returns the current fragment
* [Url::getFullQuery](#url--getfullquery) — Returns the full query
* [Url::getHostname](#url--gethostname) — Returns the hostname of the URL
* [Url::getPassword](#url--getpassword) — Returns the password in the URL, if set
* [Url::getPath](#url--getpath) — Returns the path of the URL
* [Url::getPort](#url--getport) — Returns the port, when specified in the URL
* [Url::getQuery](#url--getquery) — Returns the value of a specific query string parameter
* [Url::getScheme](#url--getscheme) — Returns the scheme (http/https)
* [Url::getUsername](#url--getusername) — Returns the username, if set in the URL
* [Url::jsonSerialize](#url--jsonserialize) — JsonSerializable::jsonSerialize — Specify data which should be serialized to JSON
* [Url::removeFragment](#url--removefragment) — Removes the #fragment in the URL
* [Url::removeFullQuery](#url--removefullquery) — Removes the ?query from the URL
* [Url::removePassword](#url--removepassword) — Removes the password in the URL
* [Url::removePort](#url--removeport) — Removes the port from the URL
* [Url::removeQuery](#url--removequery) — Removes a specific query string parameter
* [Url::removeUsername](#url--removeusername) — Removes the username from the URL; also removes the password.
* [Url::setFragment](#url--setfragment) — Sets the #fragment in the URL
* [Url::setFullQuery](#url--setfullquery) — Overwrites the query with a complete new query
* [Url::setHostname](#url--sethostname) — Sets the hostname of the URL
* [Url::setPassword](#url--setpassword) — Sets the password in the URL
* [Url::setPath](#url--setpath) — Changes the path in the URL
* [Url::setPort](#url--setport) — Sets the port
* [Url::setQuery](#url--setquery) — Sets a specific query string parameter
* [Url::setUsername](#url--setusername) — Sets the username in the URL
* [Url::__toString](#url----tostring) — Gets a string representation of the object
* [Url::current](#url--current) — Returns a URL object based on the $_SERVER global (the current request)
* [Url::__set_state](#url----set-state) — This static method is called for classes exported by `var_export()`.


<a id="url----construct"></a>
## Url::__construct
#### Description

```php
public Url::__construct(string url)
```

Makes an object based on a URL

#### Parameters

* **url**  
    The URL.

#### Errors/Exceptions

Thrown when the specified URL is invalid.

***
<a id="url--getfragment"></a>
## Url::getFragment
#### Description

```php
public Url::getFragment(): string
```

Returns the current fragment

#### Return Values

The fragment.

***
<a id="url--getfullquery"></a>
## Url::getFullQuery
#### Description

```php
public Url::getFullQuery(): array<int|string,mixed>
```

Returns the full query

#### Return Values

The full query.


***
<a id="url--gethostname"></a>
## Url::getHostname
#### Description

```php
public Url::getHostname(): string
```

Returns the hostname of the URL

#### Return Values

The hostname.

***
<a id="url--getpassword"></a>
## Url::getPassword
#### Description

```php
public Url::getPassword(): string
```

Returns the password in the URL, if set

#### Return Values

The password.

***
<a id="url--getpath"></a>
## Url::getPath
#### Description

```php
public Url::getPath(): string
```

Returns the path of the URL

#### Return Values

The path.

***
<a id="url--getport"></a>
## Url::getPort
#### Description

```php
public Url::getPort(): int
```

Returns the port, when specified in the URL

#### Return Values

The port.

***
<a id="url--getquery"></a>
## Url::getQuery
#### Description

```php
public Url::getQuery(string key): mixed|string
```

Returns the value of a specific query string parameter

#### Parameters

* **key**  
    The key.

#### Return Values

The query value.

***
<a id="url--getscheme"></a>
## Url::getScheme
#### Description

```php
public Url::getScheme(): string
```

Returns the scheme (http/https)

#### Return Values

The scheme.

***
<a id="url--getusername"></a>
## Url::getUsername
#### Description

```php
public Url::getUsername(): string
```

Returns the username, if set in the URL

#### Return Values

The username.

***
<a id="url--jsonserialize"></a>
## Url::jsonSerialize
#### Description

```php
public Url::jsonSerialize(): mixed
```

JsonSerializable::jsonSerialize — Specify data which should be serialized to JSON

#### Return Values

Returns data which can be serialized by `json_encode()`

***
<a id="url--removefragment"></a>
## Url::removeFragment
#### Description

```php
public Url::removeFragment(): self
```

Removes the #fragment in the URL

#### Return Values

The Url object.

***
<a id="url--removefullquery"></a>
## Url::removeFullQuery
#### Description

```php
public Url::removeFullQuery(): self
```

Removes the ?query from the URL

#### Return Values

The Url object.

***
<a id="url--removepassword"></a>
## Url::removePassword
#### Description

```php
public Url::removePassword(): self
```

Removes the password in the URL

#### Return Values

The Url object.

***
<a id="url--removeport"></a>
## Url::removePort
#### Description

```php
public Url::removePort(): self
```

Removes the port from the URL

#### Return Values

The Url object.

***
<a id="url--removequery"></a>
## Url::removeQuery
#### Description

```php
public Url::removeQuery(string key): self
```

Removes a specific query string parameter

#### Parameters

* **key**  
    The key.

#### Return Values

The Url object.

***
<a id="url--removeusername"></a>
## Url::removeUsername
#### Description

```php
public Url::removeUsername(): self
```

Removes the username from the URL; also removes the password.

#### Return Values

The Url object.

***
<a id="url--setfragment"></a>
## Url::setFragment
#### Description

```php
public Url::setFragment(string fragment): self
```

Sets the #fragment in the URL

#### Parameters

* **fragment**  
    The new fragment.

#### Return Values

The Url object.

***
<a id="url--setfullquery"></a>
## Url::setFullQuery
#### Description

```php
public Url::setFullQuery(array<string,mixed> query): self
```

Overwrites the query with a complete new query

#### Parameters

* **query**  
    The new query.

#### Return Values

The Url object.

***
<a id="url--sethostname"></a>
## Url::setHostname
#### Description

```php
public Url::setHostname(string hostname): self
```

Sets the hostname of the URL

#### Parameters

* **hostname**  
    The new hostname.

#### Return Values

The Url object.

***
<a id="url--setpassword"></a>
## Url::setPassword
#### Description

```php
public Url::setPassword(string password): self
```

Sets the password in the URL

#### Parameters

* **password**  
    The new password.

#### Return Values

The Url object.

***
<a id="url--setpath"></a>
## Url::setPath
#### Description

```php
public Url::setPath(string path): self
```

Changes the path in the URL

#### Parameters

* **path**  
    The new path.

#### Return Values

The Url object.

#### Errors/Exceptions

Thrown when the path doesn't start with a slash.

***
<a id="url--setport"></a>
## Url::setPort
#### Description

```php
public Url::setPort(integer port): self
```

Sets the port

#### Parameters

* **port**  
    The new port value.

#### Return Values

The Url object.

***
<a id="url--setquery"></a>
## Url::setQuery
#### Description

```php
public Url::setQuery(string key, string|mixed value): self
```

Sets a specific query string parameter

#### Parameters

* **key**  
    The key.
* **value**  
    The new value.

#### Return Values

The Url object.

***
<a id="url--setusername"></a>
## Url::setUsername
#### Description

```php
public Url::setUsername(string username): self
```

Sets the username in the URL

#### Parameters

* **username**  
    The username.

#### Return Values

The Url object.

***
<a id="url----tostring"></a>
## Url::__toString
#### Description

```php
public Url::__toString(): string
```

Gets a string representation of the object

#### Return Values

The string representation of the object.

***
<a id="url--current"></a>
## Url::current
#### Description

```php
public static Url::current(): self
```

Returns a URL object based on the $_SERVER global (the current request)

#### Return Values

The Url object.

#### Errors/Exceptions

Thrown when the method isn't called within a web request.

***
<a id="url----set-state"></a>
## Url::__set_state
#### Description

```php
public static Url::__set_state(mixed[] state): self
```

This static method is called for classes exported by `var_export()`.

#### Parameters

* **state**  
    An array containing exported properties in the form `['property' => value, ...]`.

#### Return Values

The exported object.

#### Errors/Exceptions

Thrown when there's no URL, or an invalid URL in the state.

