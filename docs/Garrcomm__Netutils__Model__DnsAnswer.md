# Model\DnsAnswer

## Class synopsis

```php
class DnsAnswer implements JsonSerializable {

    /* Methods */
    public __construct(string type, string result, integer ttl, ?integer priority)
    public getPriority(): int
    public getResult(): string
    public getTtl(): int
    public getType(): string
    public jsonSerialize(): mixed
    public __toString(): string
    public static __set_state(mixed[] state): self
}
```

<a id="methods"></a>
## Methods

* [DnsAnswer::__construct](#dnsanswer----construct) — Constructs a DNS result
* [DnsAnswer::getPriority](#dnsanswer--getpriority) — Returns the priority, if known
* [DnsAnswer::getResult](#dnsanswer--getresult) — Returns the result
* [DnsAnswer::getTtl](#dnsanswer--getttl) — Returns the amount of seconds in which the record expires
* [DnsAnswer::getType](#dnsanswer--gettype) — Returns the type of the record (One of the Dns:: constants)
* [DnsAnswer::jsonSerialize](#dnsanswer--jsonserialize) — JsonSerializable::jsonSerialize — Specify data which should be serialized to JSON
* [DnsAnswer::__toString](#dnsanswer----tostring) — Gets a string representation of the object
* [DnsAnswer::__set_state](#dnsanswer----set-state) — This static method is called for classes exported by `var_export()`.


<a id="dnsanswer----construct"></a>
## DnsAnswer::__construct
#### Description

```php
public DnsAnswer::__construct(string type, string result, integer ttl, ?integer priority)
```

Constructs a DNS result

#### Parameters

* **type**  
    One of the DNS:: constants.
* **result**  
    DNS result.
* **ttl**  
    Time to live (records expires in X seconds).
* **priority**  
    Priority of the record.

***
<a id="dnsanswer--getpriority"></a>
## DnsAnswer::getPriority
#### Description

```php
public DnsAnswer::getPriority(): int
```

Returns the priority, if known

#### Return Values

The priority or null if there's no priority.

***
<a id="dnsanswer--getresult"></a>
## DnsAnswer::getResult
#### Description

```php
public DnsAnswer::getResult(): string
```

Returns the result

#### Return Values

The DNS result

***
<a id="dnsanswer--getttl"></a>
## DnsAnswer::getTtl
#### Description

```php
public DnsAnswer::getTtl(): int
```

Returns the amount of seconds in which the record expires

#### Return Values

The time to live of the DNS record.

***
<a id="dnsanswer--gettype"></a>
## DnsAnswer::getType
#### Description

```php
public DnsAnswer::getType(): string
```

Returns the type of the record (One of the Dns:: constants)

#### Return Values

The type of the record.

***
<a id="dnsanswer--jsonserialize"></a>
## DnsAnswer::jsonSerialize
#### Description

```php
public DnsAnswer::jsonSerialize(): mixed
```

JsonSerializable::jsonSerialize — Specify data which should be serialized to JSON

#### Return Values

Returns data which can be serialized by `json_encode()`

***
<a id="dnsanswer----tostring"></a>
## DnsAnswer::__toString
#### Description

```php
public DnsAnswer::__toString(): string
```

Gets a string representation of the object

#### Return Values

The string representation of the object.

***
<a id="dnsanswer----set-state"></a>
## DnsAnswer::__set_state
#### Description

```php
public static DnsAnswer::__set_state(mixed[] state): self
```

This static method is called for classes exported by `var_export()`.

#### Parameters

* **state**  
    An array containing exported properties in the form `['property' => value, ...]`.

#### Return Values

The exported object.

#### Errors/Exceptions

Thrown when an argument is missing.

