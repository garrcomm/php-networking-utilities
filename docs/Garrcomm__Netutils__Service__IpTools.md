# Service\IpTools

## Class example

IpTools helps with basic IPv4 related stuff;

```php
$tools = new \Garrcomm\Netutils\Service\IpTools();

// getLocalIpv4s() returns a list of all IPv4 addresses on the current machine
$ips = $tools->getLocalIpv4s();
foreach ($ips as $networkName => $ip) {
    echo 'Network ' . $networkName . ' has IP address ' . $ip->getIpAddress() . PHP_EOL;
}

// networkQuickScan returns a list of all IPv4 addresses that can be found within a network
$ip = new \Garrcomm\Netutils\Model\Ipv4Address('192.168.2.1', '255.255.255.0');
$ips = $tools->networkQuickScan($ip);
foreach ($ips as $mac => $ip) {
    echo 'System ' . $mac . ' has IP address ' . $ip->getIpAddress() . PHP_EOL;
}

// With getIpByMac you can get the IP based on a MAC address
$mac = new \Garrcomm\Netutils\Model\MacAddress('aa-bb-cc-dd-ee-ff');
$ip = $tools->getIpByMac($mac);
echo 'Mac address ' . $mac . ' resolves to ' . $ip . PHP_EOL;

// With isLocalIp you can look up if an IP address is a local IP address
$ips = ['192.168.0.1', '8.8.8.8', '10.0.0.1'];
foreach ($ips as $ip) {
    echo $ip . ' is ' . ($tools->isLocalIp($ip) ? 'a' : 'not a') . ' local IP' . PHP_EOL;
}
```

## Class synopsis

```php
class IpTools {

    /* Methods */
    public __construct(string operatingSystem = 'Linux')
    public getIpByMac(MacAddress macAddress, ?Ipv4Address networkInterface = null): Ipv4Address
    public getLocalIpv4s(): array<string,Ipv4Address>
    public getMacByIp(Ipv4Address ipv4Address): MacAddress
    public isLocalIp(?integer|string|Ipv4Address ipAddress): bool
    public networkQuickScan(Ipv4Address networkInterface): array<string,Ipv4Address>
}
```

<a id="methods"></a>
## Methods

* [IpTools::__construct](#iptools----construct) — Initializes IP tools
* [IpTools::getIpByMac](#iptools--getipbymac) — Looks up the matching IP address for a MAC address.
* [IpTools::getLocalIpv4s](#iptools--getlocalipv4s) — Returns a list of all local IPv4 addresses
* [IpTools::getMacByIp](#iptools--getmacbyip) — Returns the known MAC address by its IP address.
* [IpTools::isLocalIp](#iptools--islocalip) — Returns true when the IP is a local IP
    
    The IANA has defined these IP addresses as private or local:<br>
    10.0.0.0    to 10.255.255.255,  a range that provides up to 16 million unique IP addresses.<br>
    172.16.0.0  to 172.31.255.255,  providing about 1 million unique IP addresses.<br>
    192.168.0.0 to 192.168.255.255, which offers about 65,000 unique IP addresses.
    
* [IpTools::networkQuickScan](#iptools--networkquickscan) — Returns a list of all known IP addresses in a network


<a id="iptools----construct"></a>
## IpTools::__construct
#### Description

```php
public IpTools::__construct(string operatingSystem = 'Linux')
```

Initializes IP tools

#### Parameters

* **operatingSystem**  
    The OS on which PHP is running (By default, the PHP_OS constant).

***
<a id="iptools--getipbymac"></a>
## IpTools::getIpByMac
#### Description

```php
public IpTools::getIpByMac(MacAddress macAddress, ?Ipv4Address networkInterface = null): Ipv4Address
```

Looks up the matching IP address for a MAC address.

#### Parameters

* **macAddress**  
    The MAC address to look for.
* **networkInterface**  
    The network to scan.

#### Return Values

The IP address.


***
<a id="iptools--getlocalipv4s"></a>
## IpTools::getLocalIpv4s
#### Description

```php
public IpTools::getLocalIpv4s(): array<string,Ipv4Address>
```

Returns a list of all local IPv4 addresses

#### Return Values

The IP addresses. The key is the network name.


***
<a id="iptools--getmacbyip"></a>
## IpTools::getMacByIp
#### Description

```php
public IpTools::getMacByIp(Ipv4Address ipv4Address): MacAddress
```

Returns the known MAC address by its IP address.

#### Parameters

* **ipv4Address**  
    The IP address.

#### Return Values

The MAC address.


***
<a id="iptools--islocalip"></a>
## IpTools::isLocalIp
#### Description

```php
public IpTools::isLocalIp(?integer|string|Ipv4Address ipAddress): bool
```

Returns true when the IP is a local IP

 The IANA has defined these IP addresses as private or local:<br>
 10.0.0.0    to 10.255.255.255,  a range that provides up to 16 million unique IP addresses.<br>
 172.16.0.0  to 172.31.255.255,  providing about 1 million unique IP addresses.<br>
 192.168.0.0 to 192.168.255.255, which offers about 65,000 unique IP addresses.

#### Parameters

* **ipAddress**  
    IP address.

#### Return Values

True if the IP is local.

#### Errors/Exceptions

Thrown when the input IP address is not a valid IP address at all.

***
<a id="iptools--networkquickscan"></a>
## IpTools::networkQuickScan
#### Description

```php
public IpTools::networkQuickScan(Ipv4Address networkInterface): array<string,Ipv4Address>
```

Returns a list of all known IP addresses in a network

#### Parameters

* **networkInterface**  
    The network to scan.

#### Return Values

List of IPs. The key is the MAC address as string.


