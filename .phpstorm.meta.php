<?php

namespace PHPSTORM_META {
    registerArgumentsSet(
        'dnsTypes',
        \Garrcomm\Netutils\Service\Dns::A,
        \Garrcomm\Netutils\Service\Dns::AAAA,
        \Garrcomm\Netutils\Service\Dns::TXT,
        \Garrcomm\Netutils\Service\Dns::MX,
        \Garrcomm\Netutils\Service\Dns::CNAME,
        \Garrcomm\Netutils\Service\Dns::NS,
    );
    expectedArguments(\Garrcomm\Netutils\Model\DnsAnswer::__construct(), 0, argumentsSet('dnsTypes'));
    expectedReturnValues(\Garrcomm\Netutils\Model\DnsAnswer::getType(), argumentsSet('dnsTypes'));

    expectedArguments(
        \Garrcomm\Netutils\Model\MacAddress::format(),
        1,
        \Garrcomm\Netutils\Model\MacAddress::UPPERCASE,
        \Garrcomm\Netutils\Model\MacAddress::LOWERCASE,
    );
}
