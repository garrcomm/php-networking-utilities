<?php

namespace Garrcomm\Netutils\Service;

use Garrcomm\Netutils\Model\Ipv4Address;
use Garrcomm\Netutils\Model\MacAddress;
use InvalidArgumentException;

/**
 * IpTools Service
 *
 * IpTools helps with basic IPv4 related stuff;
 * <code>
 *     $tools = new \Garrcomm\Netutils\Service\IpTools();
 *
 *     // getLocalIpv4s() returns a list of all IPv4 addresses on the current machine
 *     $ips = $tools->getLocalIpv4s();
 *     foreach ($ips as $networkName => $ip) {
 *         echo 'Network ' . $networkName . ' has IP address ' . $ip->getIpAddress() . PHP_EOL;
 *     }
 *
 *     // networkQuickScan returns a list of all IPv4 addresses that can be found within a network
 *     $ip = new \Garrcomm\Netutils\Model\Ipv4Address('192.168.2.1', '255.255.255.0');
 *     $ips = $tools->networkQuickScan($ip);
 *     foreach ($ips as $mac => $ip) {
 *         echo 'System ' . $mac . ' has IP address ' . $ip->getIpAddress() . PHP_EOL;
 *     }
 *
 *     // With getIpByMac you can get the IP based on a MAC address
 *     $mac = new \Garrcomm\Netutils\Model\MacAddress('aa-bb-cc-dd-ee-ff');
 *     $ip = $tools->getIpByMac($mac);
 *     echo 'Mac address ' . $mac . ' resolves to ' . $ip . PHP_EOL;
 *
 *     // With isLocalIp you can look up if an IP address is a local IP address
 *     $ips = ['192.168.0.1', '8.8.8.8', '10.0.0.1'];
 *     foreach ($ips as $ip) {
 *         echo $ip . ' is ' . ($tools->isLocalIp($ip) ? 'a' : 'not a') . ' local IP' . PHP_EOL;
 *     }
 * </code>
 */
class IpTools
{
    /**
     * The OS on which PHP is running (see PHP_OS constant)
     *
     * @var string
     */
    protected string $operatingSystem;

    // To be able to perform unit tests, it's possible to override these commands
    /**
     * Executable to get the IP configuration on Windows
     *
     * @var string
     */
    protected string $cmdIpconfig = 'ipconfig';
    /**
     * Executable to get the IP configuration on Linux with `ifconfig`
     *
     * @var string
     */
    protected string $cmdIfconfig = 'ifconfig';
    /**
     * Executable to get the IP configuration on Linux with `ip`
     *
     * @var string
     */
    protected string $cmdIp = 'ip';
    /**
     * Executable to get known MAC addresses
     *
     * @var string
     */
    protected string $cmdArp = 'arp';
    /**
     * Executable to ping another system
     *
     * @var string
     */
    protected string $cmdPing = 'ping';

    /**
     * Initializes IP tools
     *
     * @param string $operatingSystem The OS on which PHP is running (By default, the PHP_OS constant).
     */
    public function __construct(string $operatingSystem = PHP_OS)
    {
        $this->operatingSystem = $operatingSystem;
    }

    /**
     * Returns the known MAC address by its IP address.
     *
     * @param Ipv4Address $ipv4Address The IP address.
     *
     * @return MacAddress|null The MAC address.
     */
    public function getMacByIp(Ipv4Address $ipv4Address): ?MacAddress
    {
        if ($ipv4Address->getIpAddress() === null) {
            return null;
        }
        if ($this->operatingSystem == 'WINNT') {
            // In Windows, we can use native ICMP Ping code
            $this->nativeIcmpPing($ipv4Address->getIpAddress());
            $arpTable = $this->getArpTableWinNT();
        } else {
            // On other OSes we need to actually call the ping command
            exec(
                $this->cmdPing . ' -c 1 -s 1 -W 1 ' . $ipv4Address->getIpAddress() . ' 2>&1',
                $output,
                $returnValue
            );
            if ($returnValue > 1) { // apt install iputils-ping
                throw new \RuntimeException('Error while executing ping', $returnValue);
            }
            $arpTable = $this->getArpTableLinux();
        }

        // Returns the result
        $result = array_search($ipv4Address->getIpAddress(), $arpTable);
        return $result ? new MacAddress((string)$result) : null;
    }

    /**
     * Returns a list of all known IP addresses in a network
     *
     * @param Ipv4Address $networkInterface The network to scan.
     *
     * @return array<string,Ipv4Address> List of IPs. The key is the MAC address as string.
     */
    public function networkQuickScan(Ipv4Address $networkInterface): array
    {
        if ($this->operatingSystem == 'WINNT') {
            // In Windows, we can't broadcast ping, so we need to send ping packages to each IP.
            // To speed this up, we won't wait for the response.
            foreach ($networkInterface->getIpRange() ?? [] as $destination) {
                $this->nativeIcmpPing($destination->getIpAddress() ?? '127.0.0.1');
            }
        } else {
            // On Linux, we can send a single broadcast ping.
            exec(
                $this->cmdPing . ' -c 1 -s 1 -W 1 -b ' . $networkInterface->getBroadcastAddress() . ' 2>&1',
                $output,
                $returnValue
            );
            if ($returnValue > 1) { // apt install iputils-ping
                throw new \RuntimeException('Error while executing ping', $returnValue);
            }
        }

        // When we have pinged all hosts in the network, we can read out the ARP table
        if ($this->operatingSystem == 'WINNT') {
            $arpTable = $this->getArpTableWinNT();
        } else {
            $arpTable = $this->getArpTableLinux();
        }

        // Convert to array of IPv4Adresses
        $result = array();
        foreach ($arpTable as $mac => $ip) {
            $result[$mac] = new Ipv4Address($ip, $networkInterface->getSubnetAddress());
        }

        return $result;
    }

    /**
     * Returns true when the IP is a local IP
     *
     * The IANA has defined these IP addresses as private or local:<br>
     * 10.0.0.0    to 10.255.255.255,  a range that provides up to 16 million unique IP addresses.<br>
     * 172.16.0.0  to 172.31.255.255,  providing about 1 million unique IP addresses.<br>
     * 192.168.0.0 to 192.168.255.255, which offers about 65,000 unique IP addresses.
     *
     * @param integer|string|Ipv4Address|null $ipAddress IP address.
     *
     * @return boolean True if the IP is local.
     *
     * @throws InvalidArgumentException Thrown when the input IP address is not a valid IP address at all.
     */
    public function isLocalIp($ipAddress): bool
    {
        if ($ipAddress instanceof Ipv4Address && $ipAddress->getIpAddress() !== null) {
            $ipLong = ip2long($ipAddress->getIpAddress());
        } elseif (is_numeric($ipAddress)) {
            $ipLong = (int)$ipAddress;
            if ($ipLong < 0 || $ipLong > 4294967295) {
                throw new InvalidArgumentException('Expect a value from 0 to 4294967295. Not ' . $ipLong);
            }
        } elseif (is_string($ipAddress)) {
            $ipLong = ip2long($ipAddress);
            if ($ipLong === false) {
                throw new InvalidArgumentException('Invalid IP address specified: ' . var_export($ipAddress, true));
            }
        } else {
            throw new InvalidArgumentException('Expected int, string or Ipv4Address. Not ' . gettype($ipAddress));
        }

        if ($ipLong >= 167772160 && $ipLong <= 184549375) { // 10.*.*.*
            return true;
        }
        if ($ipLong >= 2886729728 && $ipLong <= 2887778303) { // 172.16.*.* t/m 172.31.*.*
            return true;
        }
        if ($ipLong >= 3232235520 && $ipLong <= 3232301055) { // 192.168.*.*
            return true;
        }

        return false;
    }

    /**
     * Returns an array mac=>ip based on the ARP tables on a Linux machine with 'ip neigh show' or 'arp -e'
     *
     * @return string[] The key is the MAC address, the value the IP address.
     */
    private function getArpTableLinux(): array
    {
        $cmd = $this->cmdIp . ' neigh show 2>&1';
        exec($cmd, $output, $returnValue);
        if ($returnValue == 127) {
            $cmd = $this->cmdArp . ' -an 2>&1';
            exec($cmd, $output, $returnValue);
        }
        if ($returnValue != 0) {
            throw new \RuntimeException('Error while executing ' . $cmd, $returnValue);
        }

        $return = array();
        foreach ($output as $row) {
            if (
                !preg_match(
                    '/([0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}).*?'
                    . '([0-9a-f\:]{17})/',
                    $row,
                    $matches
                )
            ) {
                continue;
            }
            $ip = $matches[1];
            $mac = strtolower($matches[2]);
            $return[$mac] = $ip;
        }

        return $return;
    }

    /**
     * Returns an array mac=>ip based on the ARP tables on a Windows machine.
     *
     * @return string[] The key is the MAC address, the value the IP address.
     */
    private function getArpTableWinNT(): array
    {
        exec($this->cmdArp . ' -a 2>&1', $output, $returnValue);
        if ($returnValue != 0) {
            throw new \RuntimeException('Error while executing arp', $returnValue);
        }

        $return = array();
        foreach ($output as $row) {
            if (
                !preg_match(
                    '/([0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}).*?'
                    . '([0-9a-f\-]{17})/',
                    $row,
                    $matches
                )
            ) {
                continue;
            }
            $ip = $matches[1];
            $mac = strtolower(str_replace('-', ':', $matches[2]));
            $return[$mac] = $ip;
        }

        return $return;
    }

    /**
     * Sends a ICMP ping with native PHP code
     *
     * @param string  $host            The remote host.
     * @param boolean $waitForResponse Set to true if you want to wait for a response.
     *
     * @return boolean
     *
     * @codeCoverageIgnore This method only works on Windows and can't be tested automatically
     */
    protected function nativeIcmpPing(string $host, bool $waitForResponse = false): bool
    {
        if (!function_exists('socket_create')) {
            throw new \RuntimeException('Native ICMP ping requires the PHP sockets module');
        }

        $protocol = getprotobyname('icmp');
        if ($protocol === false) {
            throw new \RuntimeException('Can\'t natively ping icmp');
        }

        $package = "\x08\x00\x19\x2f\x00\x00\x00\x00\x70\x69\x6e\x67";
        $socket = socket_create(AF_INET, SOCK_RAW, $protocol);
        if ($socket === false) {
            throw new \RuntimeException(
                'Can\'t create socket: ' . socket_strerror(socket_last_error()),
                socket_last_error()
            );
        }
        socket_set_option($socket, SOL_SOCKET, SO_RCVTIMEO, array('sec' => 1, 'usec' => 100));
        socket_connect($socket, $host, 0);
        $ts = microtime(true);
        socket_send($socket, $package, strLen($package), 0);
        $result = false;
        if ($waitForResponse && socket_read($socket, 255)) {
            $result = microtime(true) - $ts;
        }
        socket_close($socket);

        return (bool)$result;
    }

    /**
     * Looks up the matching IP address for a MAC address.
     *
     * @param MacAddress       $macAddress       The MAC address to look for.
     * @param Ipv4Address|null $networkInterface The network to scan.
     *
     * @return Ipv4Address|null The IP address.
     */
    public function getIpByMac(MacAddress $macAddress, Ipv4Address $networkInterface = null): ?Ipv4Address
    {
        $match = $macAddress->format(':', MacAddress::LOWERCASE);
        $networks = $networkInterface ? [$networkInterface] : $this->getLocalIpv4s();
        foreach ($networks as $network) {
            $ips = $this->networkQuickScan($network);
            if (isset($ips[$match])) {
                return $ips[$match];
            }
        }
        return null;
    }

    /**
     * Returns a list of all local IPv4 addresses
     *
     * @return array<string,Ipv4Address> The IP addresses. The key is the network name.
     */
    public function getLocalIpv4s(): array
    {
        // For Windows, use 'ipconfig'
        if ($this->operatingSystem == 'WINNT') {
            return $this->getLocalIpv4sWinNT();
        }

        // First try 'ip' utility
        try {
            return $this->getLocalIpv4sLinuxIp();
        } catch (\RuntimeException $exception) {
            if ($exception->getCode() != 127) { // command not found
                throw $exception;
            }
        }

        // When the command is not found, try the 'ifconfig' utility
        try {
            return $this->getLocalIpv4sLinuxIfconfig();
        } catch (\RuntimeException $exception) {
            if ($exception->getCode() != 127) { // command not found
                throw $exception;
            }
        }

        // When both command are not found, throw exception
        throw new \RuntimeException('Please install Linux utility "ip" or "ifconfig"', 127);
    }

    /**
     * Returns an array with all available IPv4 addresses with the Linux 'ifconfig' utility
     *
     * @return Ipv4Address[]
     */
    private function getLocalIpv4sLinuxIfconfig(): array
    {
        // Executes the ifconfig tool
        exec($this->cmdIfconfig . ' 2>&1', $output, $returnValue); // requires "apt install net-tools"
        if ($returnValue != 0) {
            throw new \RuntimeException('Error while executing ifconfig', $returnValue);
        }

        // Parses the content and convert to IP addresses whenever possible
        $ips = array();
        $networkName = 'unknown';
        foreach ($output as $row) {
            if (preg_match('/^([^\s].*?)\: /', $row, $matches)) {
                $networkName = $matches[1];
            } elseif (
                preg_match(
                    '/[\s]+inet ([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})[\s]+'
                    . 'netmask ([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})/',
                    $row,
                    $matches
                )
            ) {
                $ips[$networkName] = new Ipv4Address($matches[1], $matches[2]);
            }
        }

        return $ips;
    }

    /**
     * Returns an array with all available IPv4 addresses with the Linux 'ip' utility
     *
     * @return Ipv4Address[]
     */
    private function getLocalIpv4sLinuxIp(): array
    {
        // Executes the ip tool
        exec($this->cmdIp . ' addr 2>&1', $output, $returnValue); // requires "apt install iproute2"
        if ($returnValue != 0) {
            throw new \RuntimeException('Error while executing ip addr', $returnValue);
        }

        // Parses the content and convert to IP addresses whenever possible
        $ips = array();
        $networkName = 'unknown';
        foreach ($output as $row) {
            if (preg_match('/^([0-9]+\: .*?)\:/', $row, $matches)) {
                $networkName = $matches[1];
            } elseif (
                preg_match(
                    '/^[\s]+inet ([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\/[0-9]{1,3})/',
                    $row,
                    $matches
                )
            ) {
                $ips[$networkName] = new Ipv4Address($matches[1]);
            }
        }

        return $ips;
    }

    /**
     * Returns an array with all available IPv4 addresses with the Windows 'ipconfig' utility
     *
     * @return Ipv4Address[]
     */
    private function getLocalIpv4sWinNT(): array
    {
        // Executes the ipconfig tool
        exec($this->cmdIpconfig . ' 2>&1', $output, $returnValue);
        if ($returnValue != 0) {
            throw new \RuntimeException('Error while executing ipconfig', $returnValue);
        }

        // Parses the content
        $ipConfig = array();
        $networkName = 'unknown';
        foreach ($output as $row) {
            if (preg_match('/^   (.*?) \..*\:(.*?)$/', $row, $matches)) {
                $key = trim($matches[1], ' .');
                $ipConfig[$networkName][$key] = trim($matches[2]);
            } elseif (preg_match('/^([^\s].*?)\:$/', $row, $matches)) {
                $networkName = trim($matches[1]);
            }
        }

        // Convert to IP addresses whenever possible
        $ips = array();
        foreach ($ipConfig as $network => $properties) {
            if (isset($properties['IPv4 Address']) && isset($properties['Subnet Mask'])) {
                $ips[$network] = new Ipv4Address($properties['IPv4 Address'], $properties['Subnet Mask']);
            }
        }

        return $ips;
    }
}
