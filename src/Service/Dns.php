<?php

namespace Garrcomm\Netutils\Service;

use Garrcomm\Netutils\Model\DnsAnswer;
use Garrcomm\Netutils\Model\DnsResult;
use RuntimeException;

/**
 * Wrapper around the `nslookup` command.
 *
 * Why not using native PHP functions like `dns_get_record`? It's not possible to specify which nameserver to query.
 *
 * The Dns service wraps around `nslookup` and returns neat DNS lookup results.
 * It's also possible to change the target nameserver.
 * <code>
 *     $dns = new \Garrcomm\Netutils\Service\Dns();
 *     $dns->setDnsServer('dns.google');
 *     var_dump($dns->getMx('google.com'));
 * </code>
 */
class Dns
{
    public const
        /**
         * An IPv4 internet address (A record)
         */
        A = 'internet address',
        /**
         * An IPv6 internet address (AAAA record)
         */
        AAAA = 'AAAA address',
        /**
         * An alias to another DNS record (CNAME record)
         */
        CNAME = 'canonical name',
        /**
         * IP address(es) to an email server (MX record)
         */
        MX = 'mail exchanger',
        /**
         * DNS names of all responsible nameservers (NS record)
         */
        NS = 'nameserver',
        /**
         * A record containing plain text, used for domain verification, SPF, DKIM, etc. (TXT record)
         */
        TXT = 'text'
    ;

    /**
     * Executable to get the DNS results with `nslookup`
     *
     * @var string
     */
    protected string $cmdNslookup = 'nslookup';

    /**
     * The DNS server that's used for the queries
     *
     * @var string
     */
    protected string $dnsServer;

    /**
     * Initiates a new DNS service
     *
     * @param string $dnsServer The name server to query.
     */
    public function __construct(string $dnsServer = 'dns.google')
    {
        $this->dnsServer = $dnsServer;
    }

    /**
     * Sets the DNS server used to query all requests
     *
     * @param string $dnsServer The DNS server.
     *
     * @return void
     */
    public function setDnsServer(string $dnsServer): void
    {
        $this->dnsServer = $dnsServer;
    }

    /**
     * Returns the DNS server used to query all requests
     *
     * @return string The current DNS server.
     */
    public function getDnsServer(): string
    {
        return $this->dnsServer;
    }

    /**
     * Parses the output of `nslookup -debug -type=[type] [hostname] [dnsserver]`
     *
     * @param string $consoleOutput The output from the command line.
     *
     * @return DnsResult
     */
    protected function parseConsoleOutput(string $consoleOutput): DnsResult
    {
        // A debug result always contains specific values that are taken with this regex
        if (
            // Parses according to the Linux command line output.
            !preg_match(
                '/Server:\s+(?P<server>.*?)\s+'
                . 'Address:\s+(?P<address>[0-9.#]+).*?'
                . 'QUESTIONS:\s+(?P<questions>.*?)\n'
                . '\s+ANSWERS:\s+(?P<answers>.*?)\s+'
                . 'AUTHORITY RECORDS:/is',
                $consoleOutput,
                $consoleOutputMatches
            )
            // Parses according to the Windows command line output if the Linux variant fails.
            && !preg_match(
                '/Server:\s+(?P<server>.*?)\s+'
                . 'Address:\s+(?P<address>[0-9.#]+).*?'
                . 'QUESTIONS:\s+(?P<questions>.*?)\n'
                . '(?:\s+ANSWERS:\s+(?P<answers>.*?)\s+'
                . '-{12}|)/is',
                $consoleOutput,
                $consoleOutputMatches
            )
        ) {
            throw new RuntimeException('Invalid response from `nslookup`');
        }

        $answers = array();
        $answerTypes = [
            'text =',
            'ttl =',
            'internet address =',
            'canonical name =',
            'nameserver =',
            'mail exchanger =',
            'MX preference =',      // Windows only; the priority is mentioned separately
            'AAAA IPv6 address =',  // Windows only
            'has AAAA address',     // Linux only
        ];
        foreach (explode('->', $consoleOutputMatches['answers'] ?? '') as $answer) {
            if (!trim($answer)) {
                continue;
            }
            $ttl = 0;
            $value = '';
            $priority = null;
            $type = null;
            $answerRows = preg_split(
                '/(' . implode('|', $answerTypes) . ')\s+/ism',
                $answer,
                -1,
                PREG_SPLIT_DELIM_CAPTURE
            );
            if (!is_array($answerRows)) {
                // @codeCoverageIgnoreStart
                continue;
                // @codeCoverageIgnoreEnd
            }
            for ($answerIterator = 1; $answerIterator < count($answerRows); $answerIterator = $answerIterator + 2) {
                $answerKeyword = strtolower(trim($answerRows[$answerIterator], " \t\n\r\0\x0B="));
                $answerValue = trim($answerRows[$answerIterator + 1]);
                switch ($answerKeyword) {
                    case 'ttl':
                        $ttl = (int)$answerValue;
                        break;
                    case 'mx preference':    // MX-priority (Windows only)
                        $priority = (int)$answerValue;
                        break;
                    case 'mail exchanger':   // MX-record
                        $type = self::MX;
                        if (
                            preg_match(
                                '/(?P<priority>[0-9]+)\s+(?P<hostname>.*?)$/i',
                                $answerValue,
                                $mxMatches
                            )
                        ) { // Linux
                            $priority = (int)$mxMatches['priority'];
                            $value = rtrim($mxMatches['hostname'], '.');
                        } else { // Windows
                            $value = $answerValue;
                        }
                        if ($priority === 0 && ($value === '' || $value === '(root)')) {
                            continue 3;
                        }
                        break;
                    case 'text':             // TXT-record
                        preg_match_all('/"(.*?)"/', $answerValue, $textMatches);
                        $value .= implode(PHP_EOL, $textMatches[1]);
                        $type = self::TXT;
                        break;
                    case 'aaaa ipv6 address': // Windows specifies "AAAA IPv6 address"
                    case 'has aaaa address':  // Linux specifies "has AAAA address"
                        $type = self::AAAA;
                        $value = $answerValue;
                        break;
                    case 'nameserver':       // NS-record
                    case 'internet address': // A-record
                    case 'canonical name':   // CNAME-record
                        $type = $answerKeyword;
                        $value = rtrim($answerValue, '.');
                        break;
                    default: // This shouldn't happen; an unknown record type is returned.
                        // @codeCoverageIgnoreStart
                        throw new RuntimeException('Invalid keyword: ' . $answerKeyword);
                        // @codeCoverageIgnoreEnd
                }
            }
            if ($type === null) { // This shouldn't happen; no record type is returned.
                // @codeCoverageIgnoreStart
                continue;
                // @codeCoverageIgnoreEnd
            }
            $answers[] = new DnsAnswer(
                $type,
                $value,
                $ttl,
                $priority
            );
        }

        return new DnsResult(
            trim($consoleOutputMatches['questions']),
            $answers,
            $consoleOutputMatches['server'],
            explode('#', $consoleOutputMatches['address'], 2)[0]
        );
    }

    /**
     * Fetches all A records
     *
     * @param string $domain Hostname to get the A records from.
     *
     * @return DnsResult The A record.
     */
    public function getA(string $domain): DnsResult
    {
        return $this->parseConsoleOutput($this->nslookup('a', $domain, true));
    }

    /**
     * Fetches all AAAA records
     *
     * @param string $domain Hostname to get the AAAA records from.
     *
     * @return DnsResult The AAAA record.
     */
    public function getAAAA(string $domain): DnsResult
    {
        return $this->parseConsoleOutput($this->nslookup('aaaa', $domain, true));
    }

    /**
     * Fetches all CNAME records
     *
     * @param string $domain Hostname to get the CNAME records from.
     *
     * @return DnsResult The CNAME record.
     */
    public function getCname(string $domain): DnsResult
    {
        return $this->parseConsoleOutput($this->nslookup('cname', $domain, true));
    }

    /**
     * Fetches all MX records
     *
     * @param string $domain Hostname to get the MX records from.
     *
     * @return DnsResult The MX record.
     */
    public function getMx(string $domain): DnsResult
    {
        return $this->parseConsoleOutput($this->nslookup('mx', $domain, true));
    }

    /**
     * Fetches all NS records
     *
     * @param string $domain Hostname to get the NS records from.
     *
     * @return DnsResult The NS record.
     */
    public function getNs(string $domain): DnsResult
    {
        return $this->parseConsoleOutput($this->nslookup('ns', $domain, true));
    }

    /**
     * Fetches all TXT records
     *
     * @param string $domain Hostname to get the TXT records from.
     *
     * @return DnsResult The TXT record.
     */
    public function getTxt(string $domain): DnsResult
    {
        return $this->parseConsoleOutput($this->nslookup('txt', $domain, true));
    }

    /**
     * Executes the `nslookup` command and returns the output
     *
     * @param string  $type   The -type= parameter.
     * @param string  $domain DNS query.
     * @param boolean $debug  When true, -debug is added.
     *
     * @return string
     */
    protected function nslookup(string $type, string $domain, bool $debug = false): string
    {
        $cmd = $this->cmdNslookup . ' '
            . ($debug ? '-debug ' : '')
            . escapeshellarg('-type=' . $type) . ' '
            . escapeshellarg($domain) . ' '
            . escapeshellarg($this->dnsServer);
        exec($cmd . ' 2>&1', $output, $resultCode);
        $outputString = implode(PHP_EOL, $output);
        if ($resultCode > 0) {
            throw new RuntimeException($cmd . ' failed: ' . $outputString, $resultCode);
        }
        return $outputString;
    }
}
