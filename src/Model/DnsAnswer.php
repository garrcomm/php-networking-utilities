<?php

namespace Garrcomm\Netutils\Model;

use Garrcomm\Netutils\Service\Dns;
use JsonSerializable;
use InvalidArgumentException;

class DnsAnswer implements JsonSerializable
{
    /**
     * One of the Dns:: constants
     *
     * @var string
     */
    protected string $type;
    /**
     * The result
     *
     * @var string
     */
    protected string $result;
    /**
     * Amount of seconds in which the record expires
     *
     * @var int
     */
    protected int $ttl;
    /**
     * The priority
     *
     * @var int|null
     */
    protected ?int $priority;

    /**
     * Constructs a DNS result
     *
     * @param string       $type     One of the DNS:: constants.
     * @param string       $result   DNS result.
     * @param integer      $ttl      Time to live (records expires in X seconds).
     * @param integer|null $priority Priority of the record.
     */
    public function __construct(string $type, string $result, int $ttl, ?int $priority)
    {
        $this->type = $type;
        $this->result = $result;
        $this->ttl = $ttl;
        $this->priority = $priority;
    }

    /**
     * Returns the result
     *
     * @return string The DNS result
     */
    public function getResult(): string
    {
        return $this->result;
    }

    /**
     * Returns the amount of seconds in which the record expires
     *
     * @return integer The time to live of the DNS record.
     */
    public function getTtl(): int
    {
        return $this->ttl;
    }

    /**
     * Returns the priority, if known
     *
     * @return integer|null The priority or null if there's no priority.
     */
    public function getPriority(): ?int
    {
        return $this->priority;
    }

    /**
     * Returns the type of the record (One of the Dns:: constants)
     *
     * @return string The type of the record.
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * Gets a string representation of the object
     *
     * @return string The string representation of the object.
     *
     * @see https://www.php.net/manual/en/stringable.tostring.php
     */
    public function __toString(): string
    {
        return
            ($this->priority !== null ? $this->priority . ' ' : '')
            . $this->result;
    }

    /**
     * JsonSerializable::jsonSerialize — Specify data which should be serialized to JSON
     *
     * @return mixed Returns data which can be serialized by `json_encode()`
     *
     * @see https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        return $this->__toString();
    }

    /**
     * This static method is called for classes exported by `var_export()`.
     *
     * @param mixed[] $state An array containing exported properties in the form `['property' => value, ...]`.
     *
     * @return self The exported object.
     *
     * @throws InvalidArgumentException Thrown when an argument is missing.
     *
     * @see https://www.php.net/manual/en/language.oop5.magic.php#object.set-state
     */
    public static function __set_state(array $state): self
    {
        if (
            !isset($state['type']) || !is_string($state['type']) || !in_array($state['type'], [
                Dns::A,
                Dns::AAAA,
                Dns::CNAME,
                Dns::MX,
                Dns::NS,
                Dns::TXT,
            ])
        ) {
            throw new InvalidArgumentException('No valid type given');
        }
        if (!isset($state['result'])) {
            throw new InvalidArgumentException('No result given');
        }
        if (!isset($state['ttl'])) {
            throw new InvalidArgumentException('No ttl given');
        }
        return new self(
            $state['type'],
            $state['result'],
            $state['ttl'],
            $state['priority'] ?? null
        );
    }
}
