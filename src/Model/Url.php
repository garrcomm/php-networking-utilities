<?php

namespace Garrcomm\Netutils\Model;

use JsonSerializable;
use RuntimeException;
use InvalidArgumentException;

/**
 * This class wraps and enhances the PHP method `parse_url` so you can read and modify URLs
 *
 * <code>
 *     // You can initiate the URL class in two ways;
 *     // 1. By using the current request
 *     $url = \Garrcomm\Netutils\Model\Url::current();
 *     // 2. By using a URL in the constructor
 *     $url = new \Garrcomm\Netutils\Model\Url('https://localhost/foo?one=two&bar=baz');
 *
 *     // It's possible to set and remove elements, in one chain.
 *     echo $url
 *         ->setHostname('foo.bar')
 *         ->setPath('baz')
 *         ->setQuery('bar', 'foo')
 *         ->removeQuery('one')
 *         ->setFragment('fragment-here'); // https://foo.bar/baz?bar=foo#fragment-here
 *
 *     // The object can be treated as a string:
 *     echo '<a href="' .htmlspecialchars($url) . '">Click here</a>';
 *
 *     // The object can also be json serialized:
 *     echo json_encode($url);
 * </code>
 *
 * @see https://www.php.net/manual/en/function.parse-url.php
 */
class Url implements JsonSerializable
{
    /**
     * The URL
     *
     * @var string
     */
    protected string $url;

    /**
     * Makes an object based on a URL
     *
     * @param string $url The URL.
     *
     * @throws InvalidArgumentException Thrown when the specified URL is invalid.
     */
    public function __construct(string $url)
    {
        $parts = parse_url($url);
        if ($parts === false) {
            // @codeCoverageIgnoreStart
            throw new InvalidArgumentException('The specified URL is malformed');
            // @codeCoverageIgnoreEnd
        }
        if (!isset($parts['scheme'])) {
            if (isset($parts['port']) && $parts['port'] == 80) {
                $parts['scheme'] = 'http';
            } elseif (isset($parts['port']) && $parts['port'] == 443) {
                $parts['scheme'] = 'https';
            } else {
                throw new InvalidArgumentException('The specified URL is missing it\'s scheme');
            }
        }
        if (!isset($parts['path'])) {
            $parts['path'] = '/';
        }
        $this->url = static::glueParts($parts);
    }

    /**
     * Returns a URL object based on the $_SERVER global (the current request)
     *
     * @return self The Url object.
     *
     * @throws RuntimeException Thrown when the method isn't called within a web request.
     */
    public static function current(): self
    {
        if (!isset($_SERVER['HTTP_HOST']) || !isset($_SERVER['REQUEST_URI'])) {
            throw new RuntimeException('The current request is not a web request');
        }

        if (isset($_SERVER['HTTPS'])) {
            $url = 'https://';
            $defaultPort = 443;
        } else {
            $url = 'http://';
            $defaultPort = 80;
        }
        if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
            $url .= $_SERVER['PHP_AUTH_USER'] . ':' . $_SERVER['PHP_AUTH_PW'] . '@';
        } elseif (isset($_SERVER['PHP_AUTH_USER'])) {
            $url .= $_SERVER['PHP_AUTH_USER'] . '@';
        }
        $url .= $_SERVER['HTTP_HOST'];
        if (
            strpos($_SERVER['HTTP_HOST'], ':') === false
            && isset($_SERVER['SERVER_PORT'])
            && $_SERVER['SERVER_PORT'] != $defaultPort
        ) {
            $url .= ':' . $_SERVER['SERVER_PORT'];
        }
        $url .= $_SERVER['REQUEST_URI'];

        return new self($url);
    }

    /**
     * Returns the scheme (http/https)
     *
     * @return string The scheme.
     */
    public function getScheme(): string
    {
        return parse_url($this->url, PHP_URL_SCHEME) ?: '';
    }

    /**
     * Returns the username, if set in the URL
     *
     * @return string|null The username.
     */
    public function getUsername(): ?string
    {
        return parse_url($this->url, PHP_URL_USER) ?: null;
    }

    /**
     * Sets the username in the URL
     *
     * @param string $username The username.
     *
     * @return self The Url object.
     */
    public function setUsername(string $username): self
    {
        $parts = parse_url($this->url) ?: [];
        $parts['user'] = $username;
        $this->url = static::glueParts($parts);
        return $this;
    }

    /**
     * Removes the username from the URL; also removes the password.
     *
     * @return self The Url object.
     */
    public function removeUsername(): self
    {
        $parts = parse_url($this->url) ?: [];
        if (isset($parts['user'])) {
            unset($parts['user']);
        }
        if (isset($parts['pass'])) {
            unset($parts['pass']);
        }
        $this->url = static::glueParts($parts);
        return $this;
    }

    /**
     * Returns the password in the URL, if set
     *
     * @return string|null The password.
     */
    public function getPassword(): ?string
    {
        return parse_url($this->url, PHP_URL_PASS) ?: null;
    }

    /**
     * Sets the password in the URL
     *
     * @param string $password The new password.
     *
     * @return self The Url object.
     */
    public function setPassword(string $password): self
    {
        $parts = parse_url($this->url) ?: [];
        $parts['pass'] = $password;
        $this->url = static::glueParts($parts);
        return $this;
    }

    /**
     * Removes the password in the URL
     *
     * @return self The Url object.
     */
    public function removePassword(): self
    {
        $parts = parse_url($this->url) ?: [];
        if (isset($parts['pass'])) {
            unset($parts['pass']);
        }
        $this->url = static::glueParts($parts);
        return $this;
    }

    /**
     * Returns the hostname of the URL
     *
     * @return string The hostname.
     */
    public function getHostname(): string
    {
        return parse_url($this->url, PHP_URL_HOST) ?: '';
    }

    /**
     * Sets the hostname of the URL
     *
     * @param string $hostname The new hostname.
     *
     * @return self The Url object.
     */
    public function setHostname(string $hostname): self
    {
        $parts = parse_url($this->url) ?: [];
        $parts['host'] = $hostname;
        $this->url = static::glueParts($parts);
        return $this;
    }


    /**
     * Returns the port, when specified in the URL
     *
     * @return integer|null The port.
     */
    public function getPort(): ?int
    {
        return parse_url($this->url, PHP_URL_PORT) ?: null;
    }

    /**
     * Sets the port
     *
     * @param integer $port The new port value.
     *
     * @return self The Url object.
     */
    public function setPort(int $port): self
    {
        $parts = parse_url($this->url) ?: [];
        $parts['port'] = $port;
        $this->url = static::glueParts($parts);
        return $this;
    }

    /**
     * Removes the port from the URL
     *
     * @return self The Url object.
     */
    public function removePort(): self
    {
        $parts = parse_url($this->url) ?: [];
        if (isset($parts['port'])) {
            unset($parts['port']);
        }
        $this->url = static::glueParts($parts);
        return $this;
    }

    /**
     * Returns the path of the URL
     *
     * @return string The path.
     */
    public function getPath(): string
    {
        return parse_url($this->url, PHP_URL_PATH) ?: '';
    }

    /**
     * Changes the path in the URL
     *
     * @param string $path The new path.
     *
     * @return self The Url object.
     *
     * @throws InvalidArgumentException Thrown when the path doesn't start with a slash.
     */
    public function setPath(string $path): self
    {
        if (substr($path, 0, 1) != '/') {
            throw new InvalidArgumentException('The path must start with a /');
        }
        $parts = parse_url($this->url) ?: [];
        $parts['path'] = $path;
        $this->url = static::glueParts($parts);
        return $this;
    }

    /**
     * Returns the value of a specific query string parameter
     *
     * @param string $key The key.
     *
     * @return string|mixed|null The query value.
     */
    public function getQuery(string $key)
    {
        $query = parse_url($this->url, PHP_URL_QUERY);
        if (!is_string($query)) {
            return null;
        }
        parse_str($query, $queryParts);

        return $queryParts[$key] ?? null;
    }

    /**
     * Sets a specific query string parameter
     *
     * @param string       $key   The key.
     * @param string|mixed $value The new value.
     *
     * @return self The Url object.
     */
    public function setQuery(string $key, $value): self
    {
        $parts = parse_url($this->url) ?: [];
        parse_str($parts['query'] ?? '', $queryParts);
        $queryParts[$key] = $value;
        $parts['query'] = http_build_query($queryParts);
        $this->url = static::glueParts($parts);

        return $this;
    }

    /**
     * Removes a specific query string parameter
     *
     * @param string $key The key.
     *
     * @return self The Url object.
     */
    public function removeQuery(string $key): self
    {
        $parts = parse_url($this->url) ?: [];
        parse_str($parts['query'] ?? '', $queryParts);
        unset($queryParts[$key]);
        $parts['query'] = http_build_query($queryParts);
        $this->url = static::glueParts($parts);

        return $this;
    }

    /**
     * Returns the full query
     *
     * @return array<int|string,mixed> The full query.
     */
    public function getFullQuery(): array
    {
        $query = parse_url($this->url, PHP_URL_QUERY);
        if (!is_string($query)) {
            return [];
        }
        parse_str($query, $queryParts);
        return $queryParts;
    }

    /**
     * Overwrites the query with a complete new query
     *
     * @param array<string,mixed> $query The new query.
     *
     * @return self The Url object.
     */
    public function setFullQuery(array $query): self
    {
        $parts = parse_url($this->url) ?: [];
        $parts['query'] = http_build_query($query);
        $this->url = static::glueParts($parts);
        return $this;
    }

    /**
     * Removes the ?query from the URL
     *
     * @return self The Url object.
     */
    public function removeFullQuery(): self
    {
        return $this->setFullQuery([]);
    }

    /**
     * Returns the current fragment
     *
     * @return string|null The fragment.
     */
    public function getFragment(): ?string
    {
        $fragment = parse_url($this->url, PHP_URL_FRAGMENT);
        return $fragment ? rawurldecode((string)$fragment) : null;
    }

    /**
     * Sets the #fragment in the URL
     *
     * @param string $fragment The new fragment.
     *
     * @return self The Url object.
     */
    public function setFragment(string $fragment): self
    {
        $parts = parse_url($this->url) ?: [];
        $parts['fragment'] = rawurlencode($fragment);
        $this->url = static::glueParts($parts);

        return $this;
    }

    /**
     * Removes the #fragment in the URL
     *
     * @return self The Url object.
     */
    public function removeFragment(): self
    {
        return $this->setFragment('');
    }

    /**
     * Glues the parts from parse_url back together
     *
     * @param array<string,mixed> $parts The parts.
     *
     * @return string
     *
     * @see https://www.php.net/manual/en/function.parse-url.php
     */
    protected static function glueParts(array $parts): string
    {
        $url = '';
        if (!empty($parts['scheme'])) {
            $url .= $parts['scheme'] . '://';
        }
        if (!empty($parts['user']) && !empty($parts['pass'])) {
            $url .= $parts['user'] . ':' . $parts['pass'] . '@';
        } elseif (!empty($parts['user'])) {
            $url .= $parts['user'] . '@';
        }
        if (!empty($parts['host'])) {
            $url .= $parts['host'];
        }
        if (!empty($parts['port'])) {
            $url .= ':' . $parts['port'];
        }
        if (!empty($parts['path'])) {
            $url .= $parts['path'];
        }
        if (!empty($parts['query'])) {
            $url .= '?' . $parts['query'];
        }
        if (!empty($parts['fragment'])) {
            $url .= '#' . $parts['fragment'];
        }
        return $url;
    }

    /**
     * Gets a string representation of the object
     *
     * @return string The string representation of the object.
     *
     * @see https://www.php.net/manual/en/stringable.tostring.php
     */
    public function __toString(): string
    {
        return $this->url;
    }

    /**
     * JsonSerializable::jsonSerialize — Specify data which should be serialized to JSON
     *
     * @return mixed Returns data which can be serialized by `json_encode()`
     *
     * @see https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        return $this->__toString();
    }

    /**
     * This static method is called for classes exported by `var_export()`.
     *
     * @param mixed[] $state An array containing exported properties in the form `['property' => value, ...]`.
     *
     * @return self The exported object.
     *
     * @throws InvalidArgumentException Thrown when there's no URL, or an invalid URL in the state.
     *
     * @see https://www.php.net/manual/en/language.oop5.magic.php#object.set-state
     */
    public static function __set_state(array $state): self
    {
        if (!isset($state['url']) || !is_string($state['url'])) {
            throw new InvalidArgumentException('No URL found in the state');
        }
        return new self($state['url']);
    }
}
