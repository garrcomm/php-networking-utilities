<?php

namespace Garrcomm\Netutils\Model;

use ArrayAccess;
use Countable;
use Iterator;
use RuntimeException;
use InvalidArgumentException;
use JsonSerializable;

/**
 * This class acts as an array and can be used to walk through an IPv4 range
 *
 * You can use this object as an array:
 * <code>
 *     // Defines a range
 *     $range = new \Garrcomm\Netutils\Model\Ipv4Range('192.168.2.0', 24);
 *
 *     // Uses the Countable interface to return the amount of IPs in range
 *     echo 'Amount of IPs in the range: ' . count($range) . PHP_EOL;
 *
 *     // Uses the Iterator interface to walk through the results
 *     foreach ($range as $ip) {
 *         echo '- ' . $ip . PHP_EOL;
 *     }
 *
 *     // Uses the ArrayAccess interface to walk through the results
 *     for ($i = 0; $i < count($range); ++$i) {
 *         echo '- ' . $range[$i] . PHP_EOL;
 *     }
 *
 *     // Returns a json array with all IPs in range
 *     echo json_encode($range, JSON_PRETTY_PRINT);
 *
 *     // Returns the IP range as string
 *     echo $range . PHP_EOL;
 * </code>
 *
 * @implements ArrayAccess<int, Ipv4Address>
 * @implements Iterator<int, Ipv4Address>
 */
class Ipv4Range implements Iterator, Countable, ArrayAccess, JsonSerializable
{
    /**
     * Current iterator position in the array
     *
     * @var integer
     */
    protected int $position = 0;
    /**
     * The network address
     *
     * @var integer
     */
    protected int $networkAddress;
    /**
     * The bit mask
     *
     * @var integer
     */
    protected int $bitMask;

    /**
     * Constructs an IPv4 range
     *
     * @param string|integer $networkAddress The network address.
     * @param integer        $bitMask        The bit mask.
     *
     * @throws InvalidArgumentException Thrown when the arguments are no valid IP addresses.
     */
    public function __construct($networkAddress, int $bitMask)
    {
        if (preg_match('/^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$/', (string)$networkAddress)) {
            $networkAddress = ip2long((string)$networkAddress);
        }
        if (!is_numeric($networkAddress)) {
            throw new InvalidArgumentException('networkAddress must be a string or an integer');
        }

        // When a normal IP address is used, calculate it back to the network address.
        $this->bitMask = $bitMask;
        $this->networkAddress = $networkAddress & $this->getSubnetAddress();
    }

    /**
     * Returns the subnet address based on the bitmask
     *
     * @return integer
     */
    protected function getSubnetAddress(): int
    {
        return 0xffffffff << (32 - $this->bitMask);
    }

    /**
     * Iterator::current — Return the current element
     *
     * @return mixed Can return any type.
     *
     * @see https://www.php.net/manual/en/iterator.current.php
     */
    #[\ReturnTypeWillChange]
    public function current()
    {
        return $this->offsetGet($this->position);
    }

    /**
     * Iterator::next — Move forward to next element
     *
     * @return void
     *
     * @see https://www.php.net/manual/en/iterator.next.php
     */
    #[\ReturnTypeWillChange]
    public function next()
    {
        ++$this->position;
    }

    /**
     * Iterator::key — Return the key of the current element
     *
     * @return mixed Returns scalar on success, or null on failure.
     *
     * @see https://www.php.net/manual/en/iterator.key.php
     */
    #[\ReturnTypeWillChange]
    public function key()
    {
        return $this->position;
    }

    /**
     * Iterator::valid — Checks if current position is valid
     *
     * @return boolean The return value will be casted to bool and then evaluated.
     *
     * @see https://www.php.net/manual/en/iterator.valid.php
     */
    public function valid(): bool
    {
        return $this->offsetExists($this->position);
    }

    /**
     * Iterator::rewind — Rewind the Iterator to the first element
     *
     * @return void
     *
     * @see https://www.php.net/manual/en/iterator.rewind.php
     */
    #[\ReturnTypeWillChange]
    public function rewind()
    {
        $this->position = 0;
    }

    /**
     * Countable::count — Count elements of an object
     *
     * @return integer The custom count as an int.
     *
     * @see https://www.php.net/manual/en/countable.count.php
     */
    public function count(): int
    {
        return (1 << (32 - $this->bitMask));
    }

    /**
     * ArrayAccess::offsetExists — Whether an offset exists
     *
     * @param mixed $offset An offset to check for.
     *
     * @return boolean Returns true on success or false on failure.
     *
     * @see https://www.php.net/manual/en/arrayaccess.offsetexists.php
     */
    public function offsetExists($offset): bool
    {
        return is_numeric($offset) && $offset >= 0 && $offset < $this->count();
    }

    /**
     * ArrayAccess::offsetGet — Offset to retrieve
     *
     * @param mixed $offset The offset to retrieve.
     *
     * @return mixed Can return all value types.
     *
     * @see https://www.php.net/manual/en/arrayaccess.offsetget.php
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        if (!$this->offsetExists($offset)) {
            return null;
        }
        return new Ipv4Address($this->networkAddress + $offset, $this->getSubnetAddress());
    }

    /**
     * ArrayAccess::offsetSet — Assign a value to the specified offset
     *
     * @param mixed $offset The offset to assign the value to.
     * @param mixed $value  The value to set.
     *
     * @return void
     *
     * @throws RuntimeException Thrown because this object is read only.
     *
     * @see https://www.php.net/manual/en/arrayaccess.offsetset.php
     */
    public function offsetSet($offset, $value): void
    {
        throw new RuntimeException('The ' . __CLASS__ . ' object is read only');
    }

    /**
     * ArrayAccess::offsetUnset — Unset an offset
     *
     * @param mixed $offset The offset to unset.
     *
     * @return void
     *
     * @throws RuntimeException Thrown because this object is read only.
     *
     * @see https://www.php.net/manual/en/arrayaccess.offsetunset.php
     */
    public function offsetUnset($offset): void
    {
        throw new RuntimeException('The ' . __CLASS__ . ' object is read only');
    }

    /**
     * Gets a string representation of the object
     *
     * @return string The string representation of the object.
     *
     * @see https://www.php.net/manual/en/stringable.tostring.php
     */
    public function __toString(): string
    {
        return long2ip($this->networkAddress) . '/' . $this->bitMask . '-'
            . long2ip($this->networkAddress + $this->count() - 1) . '/' . $this->bitMask;
    }

    /**
     * JsonSerializable::jsonSerialize — Specify data which should be serialized to JSON
     *
     * @return mixed Returns data which can be serialized by `json_encode()`
     *
     * @see https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        $ips = array();
        for ($ip = $this->networkAddress; $ip < $this->networkAddress + $this->count(); ++$ip) {
            $ips[] = long2ip($ip) . '/' . $this->bitMask;
        }
        return $ips;
    }

    /**
     * This static method is called for classes exported by `var_export()`.
     *
     * @param mixed[] $state An array containing exported properties in the form `['property' => value, ...]`.
     *
     * @return self The exported object.
     *
     * @throws InvalidArgumentException Thrown when the state is incomplete.
     *
     * @see https://www.php.net/manual/en/language.oop5.magic.php#object.set-state
     */
    public static function __set_state(array $state): self
    {
        // Validate the minimum requirements and constructs the range
        if (
            !isset($state['networkAddress'])
            || (!is_int($state['networkAddress']) && !is_string($state['networkAddress']))
        ) {
            throw new InvalidArgumentException('No networkAddress found in the state');
        }
        if (!isset($state['bitMask']) || !is_int($state['bitMask'])) {
            throw new InvalidArgumentException('No bitMask found in the state');
        }
        $return = new self($state['networkAddress'], $state['bitMask']);

        // Set the position, if known
        if (isset($state['position']) && is_int($state['position'])) {
            $return->position = $state['position'];
        }

        return $return;
    }
}
