<?php

declare(strict_types=1);

namespace Garrcomm\Netutils\Model;

use InvalidArgumentException;

/**
 * IPv4 Address data model
 *
 * The `Ipv4Address` model is also used in the `IpTools` service, but it can also help with different notations
 * and even calculate complete subnets;
 * <code>
 *     $ip = new \Garrcomm\Netutils\Model\Ipv4Address('192.168.1.1', '255.255.255.0');
 *     echo 'CIDR notation:     ' . $ip->getCidrAddress() . PHP_EOL;
 *     echo 'Broadcast address: ' . $ip->getBroadcastAddress() . PHP_EOL;
 *     echo 'Network address:   ' . $ip->getNetworkAddress() . PHP_EOL;
 *     echo 'Amount of IP\'s in range: ' . count($ip->getIpRange()) . PHP_EOL;
 * </code>
 */
class Ipv4Address implements \JsonSerializable
{
    /**
     * The IP address
     *
     * @var null|int
     */
    private ?int $ipLong = null;

    /**
     * The subnet address
     *
     * @var null|int
     */
    private ?int $subnetLong = null;

    /**
     * Creates a new IP entity
     *
     * Input can be an IP address as integer, regular, or in CIDR format.
     *
     * @param string|integer|null $ipAddress     IP address, different formats are supported.
     * @param string|integer|null $subnetAddress Subnet address, different formats are supported.
     *
     * @throws InvalidArgumentException An exception will be thrown when the IP address is invalid.
     */
    public function __construct($ipAddress = null, $subnetAddress = null)
    {
        // Sets the IP (if specified)
        if (is_string($ipAddress) && strpos($ipAddress, "/") !== false) {
            $this->setCidrAddress($ipAddress);
        } elseif ($ipAddress !== null) {
            $this->setIpAddress($ipAddress);
        }

        // Sets the subnet (if specified)
        if ($subnetAddress !== null) {
            $this->setSubnetAddress($subnetAddress);
        }
    }

    /**
     * Sets the IP address
     *
     * @param string|integer|null $ipAddress The IP address as long or in dotted notation.
     *
     * @return Ipv4Address The Ipv4Address object.
     *
     * @throws InvalidArgumentException Throws an exception when the input is invalid.
     */
    public function setIpAddress($ipAddress): self
    {
        // Sets the IP (if specified)
        if ($ipAddress === null) {
            $this->ipLong = null;
        } elseif (is_numeric($ipAddress)) {
            $this->ipLong = (int)$ipAddress;
        } elseif ($this->validateIp($ipAddress) && ip2long($ipAddress)) {
            $this->ipLong = ip2long($ipAddress);
        } else {
            throw new InvalidArgumentException("Invalid IP format: " . $ipAddress);
        }

        return $this;
    }

    /**
     * Sets the subnet address
     *
     * @param string|integer|null $subnetAddress The subnet address as long or in dotted notation.
     *
     * @return Ipv4Address The Ipv4Address object.
     *
     * @throws InvalidArgumentException Throws an exception when the input is invalid.
     */
    public function setSubnetAddress($subnetAddress): self
    {
        // Sets the IP (if specified)
        if ($subnetAddress === null) {
            $this->subnetLong = null;
        } elseif (is_numeric($subnetAddress)) {
            $this->subnetLong = (int)$subnetAddress;
        } elseif ($this->validateIp($subnetAddress) && ip2long($subnetAddress)) {
            $this->subnetLong = ip2long($subnetAddress);
        } else {
            throw new InvalidArgumentException("Invalid subnet format: " . $subnetAddress);
        }

        return $this;
    }

    /**
     * Fills this entity with an IP address based on CIDR notation
     *
     * @param string $ipMask The IP address in CIDR notation (127.0.0.1/8).
     *
     * @return Ipv4Address The Ipv4Address object.
     *
     * @throws InvalidArgumentException An exception will be thrown when the IP address is invalid.
     */
    public function setCidrAddress(string $ipMask): self
    {
        if (strpos($ipMask, "/") === false) {
            throw new InvalidArgumentException("Invalid CIDR format: " . $ipMask);
        }

        // Splits IP and mask bits
        list($ip, $bitMask) = explode('/', $ipMask, 2);
        if (!$this->validateIp($ip) || !is_numeric($bitMask) || !ip2long($ip)) {
            throw new InvalidArgumentException("Invalid CIDR format: " . $ipMask);
        }

        // Performs calculations
        $this->ipLong = ip2long($ip);
        $this->setBitMask((int)$bitMask);

        return $this;
    }

    /**
     * Sets the bitmask presentation of the subnet
     *
     * @param integer $bitMask The bitmask.
     *
     * @return Ipv4Address The Ipv4Address object.
     */
    public function setBitMask(int $bitMask): self
    {
        $this->subnetLong = 0xffffffff << (32 - $bitMask);

        return $this;
    }

    /**
     * Returns the IP address in CIDR notation
     *
     * @return string|null The IP address in CIDR notation or null when there's no IP.
     */
    public function getCidrAddress(): ?string
    {
        if ($this->ipLong === null) {
            return null;
        }
        return $this->getIpAddress() . '/' . $this->getBitMask();
    }

    /**
     * Returns the IP address
     *
     * @return string|null The IP address, or null when it's empty.
     */
    public function getIpAddress(): ?string
    {
        if ($this->ipLong === null) {
            return null;
        }
        return long2ip($this->ipLong) ?: null;
    }

    /**
     * Returns the full IP range in this network
     *
     * @return Ipv4Address[]|Ipv4Range|null The list of IP addresses in range.
     */
    public function getIpRange(): ?Ipv4Range
    {
        $networkAddress = $this->getNetworkAddress();
        $bitmask = $this->getBitMask();
        if ($networkAddress === null || $bitmask === null) {
            return null;
        }
        return new Ipv4Range($networkAddress, $bitmask);
    }

    /**
     * Returns the network address
     *
     * @return string|null The network address.
     */
    public function getNetworkAddress(): ?string
    {
        if ($this->subnetLong === null || $this->ipLong === null) {
            return null;
        }
        return long2ip($this->ipLong & $this->subnetLong) ?: null;
    }

    /**
     * Returns the subnet address
     *
     * @return string|null The subnet address.
     */
    public function getSubnetAddress(): ?string
    {
        if ($this->subnetLong === null) {
            return null;
        }
        return long2ip($this->subnetLong) ?: null;
    }

    /**
     * Returns the broadcast address
     *
     * @return string|null The broadcast address.
     */
    public function getBroadcastAddress(): ?string
    {
        if ($this->subnetLong === null || $this->ipLong === null) {
            return null;
        }
        return long2ip($this->ipLong | (~$this->subnetLong)) ?: null;
    }

    /**
     * Returns the bitmask presentation of the subnet
     *
     * @return integer|null The bitmask.
     */
    public function getBitMask(): ?int
    {
        if ($this->subnetLong === null) {
            return null;
        }
        $int = $this->subnetLong & 0xFFFFFFFF;
        $int = ( $int & 0x55555555 ) + ( ( $int >> 1 ) & 0x55555555 );
        $int = ( $int & 0x33333333 ) + ( ( $int >> 2 ) & 0x33333333 );
        $int = ( $int & 0x0F0F0F0F ) + ( ( $int >> 4 ) & 0x0F0F0F0F );
        $int = ( $int & 0x00FF00FF ) + ( ( $int >> 8 ) & 0x00FF00FF );
        $int = ( $int & 0x0000FFFF ) + ( ( $int >> 16 ) & 0x0000FFFF );
        $int = $int & 0x0000003F;

        return $int;
    }

    /**
     * Validates an IP address
     *
     * @param string $ip IP address in dotted format.
     *
     * @return boolean
     */
    private function validateIp(string $ip): bool
    {
        return preg_match('/^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$/', $ip) === 1;
    }

    /**
     * Gets a string representation of the object
     *
     * @return string The string representation of the object.
     *
     * @see https://www.php.net/manual/en/stringable.tostring.php
     */
    public function __toString(): string
    {
        return $this->getCidrAddress() ?? '-';
    }

    /**
     * JsonSerializable::jsonSerialize — Specify data which should be serialized to JSON
     *
     * @return mixed Returns data which can be serialized by `json_encode()`
     *
     * @see https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        return $this->__toString();
    }

    /**
     * This static method is called for classes exported by `var_export()`.
     *
     * @param mixed[] $state An array containing exported properties in the form `['property' => value, ...]`.
     *
     * @return self The exported object.
     *
     * @see https://www.php.net/manual/en/language.oop5.magic.php#object.set-state
     */
    public static function __set_state(array $state): self
    {
        if (isset($state['ipLong']) && !is_numeric($state['ipLong'])) {
            throw new InvalidArgumentException('ipLong must be numeric');
        }
        if (isset($state['subnetLong']) && !is_numeric($state['subnetLong'])) {
            throw new InvalidArgumentException('subnetLong must be numeric');
        }
        return new self(
            $state['ipLong'] ?? null,
            $state['subnetLong'] ?? null,
        );
    }
}
