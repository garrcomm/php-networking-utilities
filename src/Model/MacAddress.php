<?php

declare(strict_types=1);

namespace Garrcomm\Netutils\Model;

use InvalidArgumentException;

class MacAddress implements \JsonSerializable
{
    public const UPPERCASE = 1;
    public const LOWERCASE = 0;

    /**
     * MAC address without hyphens, colons, etc.
     *
     * @var string
     */
    protected string $macAddress;

    /**
     * Initializes a new MAC address model.
     *
     * @param string $macAddress The MAC address.
     */
    public function __construct(string $macAddress)
    {
        $this->set($macAddress);
    }

    /**
     * Set a new value
     *
     * @param string $macAddress The new value.
     *
     * @return MacAddress
     */
    public function set(string $macAddress): self
    {
        // Removes hyphens, colons, etc.
        $cleanup = strtoupper(str_replace([':', '-'], ['', ''], $macAddress));
        // Validate value
        if (!preg_match('/^[0-9A-F]{12}$/', $cleanup)) {
            throw new \InvalidArgumentException('Invalid MAC address format');
        }
        // Set value
        $this->macAddress = $cleanup;

        return $this;
    }

    /**
     * Get a string representation of the MAC address
     *
     * @param string  $separator Default ':', but can also be '-'.
     * @param integer $casing    Should be self:LOWERCASE or self:UPPERCASE.
     *
     * @return string
     */
    public function format(string $separator = ':', int $casing = self::LOWERCASE): string
    {
        $return = implode($separator, str_split($this->macAddress, 2));
        return $casing === self::LOWERCASE ? strtolower($return) : $return;
    }

    /**
     * Gets a string representation of the object
     *
     * @return string The string representation of the object.
     *
     * @see https://www.php.net/manual/en/stringable.tostring.php
     */
    public function __toString(): string
    {
        return $this->format('', self::UPPERCASE);
    }

    /**
     * JsonSerializable::jsonSerialize — Specify data which should be serialized to JSON
     *
     * @return mixed Returns data which can be serialized by `json_encode()`
     *
     * @see https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        return $this->format();
    }

    /**
     * This static method is called for classes exported by `var_export()`.
     *
     * @param mixed[] $state An array containing exported properties in the form `['property' => value, ...]`.
     *
     * @return self The exported object.
     *
     * @throws InvalidArgumentException Thrown when the state is incomplete.
     *
     * @see https://www.php.net/manual/en/language.oop5.magic.php#object.set-state
     */
    public static function __set_state(array $state): self
    {
        if (!isset($state['macAddress']) || !is_string($state['macAddress'])) {
            throw new InvalidArgumentException('No macAddress found in the state');
        }
        return new self($state['macAddress']);
    }
}
