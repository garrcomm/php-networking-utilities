<?php

namespace Garrcomm\Netutils\Model;

use JsonSerializable;
use InvalidArgumentException;

class DnsResult implements JsonSerializable
{
    /**
     * The original question
     *
     * @var string
     */
    protected string $question;
    /**
     * List of all answers
     *
     * @var DnsAnswer[]
     */
    protected array $answers;

    /**
     * The hostname of the DNS server.
     *
     * @var string
     */
    protected string $dnsHostname;
    /**
     * The address of the DNS server.
     *
     * @var string
     */
    protected string $dnsAddress;

    /**
     * Constructs a DNS result
     *
     * @param string      $question    The original question.
     * @param DnsAnswer[] $answers     All DNS answers.
     * @param string      $dnsHostname The hostname of the DNS server.
     * @param string      $dnsAddress  The address of the DNS server.
     */
    public function __construct(
        string $question,
        array $answers,
        string $dnsHostname,
        string $dnsAddress
    ) {
        $this->question = $question;
        $this->answers = $answers;
        $this->dnsHostname = $dnsHostname;
        $this->dnsAddress = $dnsAddress;
    }

    /**
     * Returns the original DNS question
     *
     * @return string The original question.
     */
    public function getQuestion(): string
    {
        return $this->question;
    }

    /**
     * Returns all given answers
     *
     * @param boolean $sorted Set to true to sort the answers. Useful to sort MX records by priority for example.
     *
     * @return DnsAnswer[] A list of all answers.
     */
    public function getAnswers(bool $sorted = false): array
    {
        if ($sorted) {
            $return = array();
            foreach ($this->answers as $answer) {
                $return[($answer->getPriority() ?? '0') . $answer->getResult()] = $answer;
            }
            ksort($return);
            return array_values($return);
        }
        return $this->answers;
    }

    /**
     * Returns the hostname of the DNS server.
     *
     * @return string The DNS hostname.
     */
    public function getDnsHostname(): string
    {
        return $this->dnsHostname;
    }

    /**
     * The address of the DNS server.
     *
     * @return string The DNS IP address.
     */
    public function getDnsAddress(): string
    {
        return $this->dnsAddress;
    }

    /**
     * Gets a string representation of the object
     *
     * @return string The string representation of the object.
     *
     * @see https://www.php.net/manual/en/stringable.tostring.php
     */
    public function __toString(): string
    {
        return implode(PHP_EOL, $this->answers);
    }

    /**
     * JsonSerializable::jsonSerialize — Specify data which should be serialized to JSON
     *
     * @return mixed Returns data which can be serialized by `json_encode()`
     *
     * @see https://www.php.net/manual/en/jsonserializable.jsonserialize.php
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        return [
            'server' => $this->dnsHostname,
            'question' => $this->question,
            'answers' => $this->answers,
        ];
    }

    /**
     * This static method is called for classes exported by `var_export()`.
     *
     * @param mixed[] $state An array containing exported properties in the form `['property' => value, ...]`.
     *
     * @return self The exported object.
     *
     * @throws InvalidArgumentException Thrown when an argument is missing.
     *
     * @see https://www.php.net/manual/en/language.oop5.magic.php#object.set-state
     */
    public static function __set_state(array $state): self
    {
        if (!isset($state['question']) || !is_string($state['question'])) {
            throw new InvalidArgumentException('No question given');
        }
        if (!isset($state['answers']) || !is_array($state['answers'])) {
            throw new InvalidArgumentException('No answers given');
        }
        if (!isset($state['dnsHostname']) || !is_string($state['dnsHostname'])) {
            throw new InvalidArgumentException('No dnsHostname given');
        }
        if (!isset($state['dnsAddress']) || !is_string($state['dnsAddress'])) {
            throw new InvalidArgumentException('No dnsAddress given');
        }
        return new self(
            $state['question'],
            $state['answers'],
            $state['dnsHostname'],
            $state['dnsAddress']
        );
    }
}
